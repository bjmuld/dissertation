% !TeX encoding = UTF-8
% !TeX root = ../../muldrey_thesis.tex
% !TeX TS-program = pdflatex
% !BIB TS-program = bibtex
% !TeX spellcheck = en_US

\section{Approach}

\begin{figure}
	\centering
	\includegraphics[width=4.1in]{plots/overview}
	\caption{Overview of proposed validation methodology.}
	\labelfig{overview}
\end{figure}

\reffig{overview} gives an overview of the proposed validation approach.
A database of observations consisting of the common- and differential-mode responses
of the two models is built through a bootstrapping technique.
In order to begin constructing an observation database, we must first estimate $m$, the amount of memory present in the system so that our state-space model will be of sufficient dimensionality.
Based on the result of a memory-estimation procedure (Step 1 in \reffig{overview} and detailed in \refsec{memory}), the observations in the database can be decomposed into $<state, input, response>$ triplets.
Step two is bootstrapping, wherein
batches of uniformly random stimuli (transient time domain waveforms consisting of piecewise-linear segments) are used to concurrently stimulate both the high and low level models of the analog circuits being validated.
Initially there are no learning kernels present in the high level model.
Statistical density models of the observed common-mode and differential-mode states and responses in the database are constructed.
After each batch of random stimuli is simulated and the observations are collected, decomposed, and added to the database, we update the density models.

% The usefullness of the last batch is proportional to the degree of change in the density models, measured as the Kullbeck-Leibler divergence (KLD) between the previous and updated density models.
% The accumulated KLD over all bootstrapping represents the total amount of information gathered.
% So, the bootstrapping phase terminates when:
% \begin{equation}
%     \frac{\text{KLD}_0}{\int\limits_i \text{KLD}_i} < \text{2\%}
% \end{equation}
% where $\text{KLD}_0$ is the most recent measurement.

Because any learning is entirely dependent upon training data, and we are primarily interested in
the ways our models differ, we must insure our database contains a sufficient proportion of this useful information.
If the models are highly similar, then it follows that the majority of observations are of little use.
At step 3, we quantify the proportion of high disagreement observations in our data.
To gather additional \textit{useful} observations, in step 4, we sample from the tails of the
differential-mode density model (the regions of the state-space where
we observe the greatest disagreement between the models), project stimuli likely to
drive the system into those regions, and simulate them.
As the additional simulations are conducted, the statistics of disagreement across all
observations in the database are recalculated.
% resampling continues until the 4\textsuperscript{th} statistical moment, \textit{kurtosis}, is close to normal (3).
Finally, in step 5, we enhance our high-level model by training a machine-learning network on our data and attaching it in parallel with the model.
The database is then updated to reflect the respose of the updated model, and its statistical models are rebuilt.
The process is repeated, augmenting the high level model with learning kernels in each learning iteration until the disagreement statistics approximates a normal distribution with a very low (specified) variance. At this point the algorithm terminates with the automatically generated augmented high level model
matching the behavior of the low level model across its entire input space.


% In the \textit{i'th} iteration of the algorithm (learning iteration), the \textit{i'th} learning kernel is appended to the high level model (keeping learning kernels from prior iterations intact) and trained to produce the phase-inverted difference between the responses produced by the high and low level circuit models to the population of stimuli of \reffig{overview}.


\textit{Preliminaries:} The entire suite of algorithms developed is structured around making repeated joint observations on two (or more) systems, each modeled as a partially-observable Markov decision process (POMDP) \cite{astromOptimalControlMarkov1965}, with the goal being to generate the largest proportion of useful information in as efficient a manner as possible.
The POMDP formulation requires the careful discretization of time:
%based on the dynamics of the system, 
a period is chosen for which
input signals are changed and the states of the system are recalculated
(a continuous-time transient simulation can occur within a timestep).
%Additionally, the (PO)MDP models are treated as black-boxes; full system-state is unknown and uncontrollable beyond %the input signal.
%And so,
Each input signal must begin at a known reset condition and must span a sufficient number of time periods for the constituent systems to enter new states.
At which time an additional input is given, and the responses are recorded.
The entire observation, $\bm{O}_i$, is thus recorded as a 3-tuple composed of:
1) the system state estimate, $\bm{S}_i$, a vector of length $mb_i + (m-1)b_o$ ($b_i$ representing dimensionality of inputs; $b_o$, dimensionality of outputs);
2) the most recent input, $\bm{A}_i$, (``action'' in MDP parlance);
and 3) the system's response, $\bm{R}_i$ (output in the most recent time step).
Additionally, we define an input vector, $\bm{x}$, composed of $\bm{S}$ and $\bm{A}$, and a response vector, $ \bm{y}$, equivalent to $\bm{R}$: $ \bm{O}_i \coloneqq{ \{ \bm{S}_i, \bm{A}_i, \bm{R}_i \} }$,
$ \bm{x}_i \coloneqq{ \{ \bm{S}_i, \bm{A}_i           \} }$ and 
$\bm{y}_i \coloneqq{ \{ \bm{R}_i                     \} }$.
% A population of stimulus $\bm{x}$ is used to excite the high and low level models of
% \reffig{overview} and histograms (probability density functions with selected kernels) are
% constructed. The rest of the algorithmic functions are described in  \reffig{flow}.

\begin{figure}
	\includegraphics[width=2.15in, height=3.0in ]{plots/flow}
	\centering
	\caption{Validation methodology: algorithmic steps}
	\labelfig{flow}
\end{figure}

% The first step in \reffig{flow} is memory order estimation.
% This determines the duration of each stimulusnecessary to capture the memory latency of the circuit being validated.
% The second step involves creating a statistical representation of the system response disagreements to the applied population of input stimuli (e.g. final system states or outputs observed after application of an input stimulus for the entire stimulus population or extracted frequency domain features).
% The third step involves identifying tails of the corresponding probability density functions from measurements of excess kurtosis of the same.
% If no such tails are found, the process is terminated.
% The fourth step involves creation of a set of stimuli using tail region resampling algorithms (using Kriging techniques) that are most likely to excite tail disagreement behaviors.
% The final step involves augmenting the high level model with trained learning kernels that null the disagreement statistics over the population of stimuli simulated.
% This also involves updating the stimulus population with the stimuli identified from the tail region resampling techniques discussed in the prior step. The process is repeated from the second step until
% it terminates. In the following, each of the steps above is discussed in greater detail.

%\begin{gather}
    %\bm{O}_i \coloneqq{ \{ \bm{S}_i, \bm{A}_i, \bm{R}_i \} } \\
    %\bm{x}_i \coloneqq{ \{ \bm{S}_i, \bm{A}_i           \} } \\
    %\bm{y}_i \coloneqq{ \{ \bm{R}_i                     \} }
%\end{gather}

\begin{figure}
	\centering
	\includegraphics[width=3.0in]{inkscape/composeTables}
	\caption{Composition of Observation Database}
	%\Description{A block diagram indicating the control-flow of the algorithm}
	\labelfig{datacomp}
\end{figure}

In this fashion, arbitrary input and output observations (length $n$) of a system with hypothetical memory $m$ can be decomposed into $n$ experiments by convolving a window of length $m$ across the input and output time-series,
and recording the $y$ value corresponding to time point $[m+1]$ relative to each window.
\reffig{datacomp} illustrates the concept.


%The following summarize the steps of the proposed algorithm and d%iscuss the information available at each %point:

%\paragraph{1. Estimate the memory-order of the %system-difference.}
%Without any a priori knowledge of the nature of the discrepancy %between 
%systems,
%we do know the upper limit of memory either system is capable %of 
%exhibiting either as a
%function of capacitance or of explicit digital/ discrete-time %storage. 
% And so, we can infer an upper bound on the amount of memory %any 
% perceptible difference might exhibit as the maximum memory of %either system (where $\mathcal{M}$ is the %memory operator and $S_i$ is a system under observation).
%With an upper bound in place we explore the probability that a %finite memory greater than zero and less %than the upper bound %exists within which all differential dynamics can be captured.

%\begin{equation}
%%\max_i \{ \ \mathcal{M}(S_i) \ \}
%\end{equation}


%\paragraph{2. Build a bootstrapped statistical model of the common- and differential-system behaviors.}
%Perform repeated experiments on the system with inputs generated from
%a uniform sampling of the input space.
%Simulating from reset at time $t=0$ to $t=2m$ , build a statistical model of the multidimensional output
%observed in the transformed space ${ \frac{1}{2}(y_2+y_1), \frac{1}{2}(y_2-y_1)}$
%and halting when continued experimentation yields insignificant
%updates to the model.


%\paragraph{3. Quantify the tails of the error distribution.}
% Identify the presence of outliers or excess kurtosis in the model error 
% distribution.
%We may have observations which directly reveal uncharacteristically high disagreement (in excess of some %n-sigma threshold, for example) or alternatively, a number of samples supporting a “long tail” hypothesis %on the error distribution (equivalent to a high degree of kurtosis).
%In this case, we can conclude that there exists at least one corner of operational dynamics in which our %models are in significant disagreement.


%\paragraph{4. Resample the systems in tail regions.}
%Look into the tail regions and into the neighborhoods of existing tail data-points and identify candidate %simulations using an adapted Kriging approach for high-dimensional data, and conduct additional %experiments in the regions around existing tail data in order to gain observational knowledge about the %systems' behaviors in these exceptionally discordant regions.


%\paragraph{5. Build boosted models which capture the discrepancy between the two systems.}
%Using all available data, boost existing machine-learning models at the outputs of one (or both) systems, %capturing the observed differences.  Because tail data is given an emphasis in the data collection phase, %it will therefore have an emphasis during model building in proportion to the number of tail samples %included in the training.


%Finally, we return to step 2 and quantify the distribution of disagreement once again.
%If we continue to observe high-disagreement, low-likelihood regions, we can continue the procedure of %resampling and training predictive models. 
%If, however, we believe the kurtosis of the error distribution is well controlled, we can terminate this %algorithm and begin exploring hypotheses in which we move the functionality of the boosting models into %the interior of either or both system models.







\subsection{Memory Estimation Algorithm}
\labelsec{memory}


In this algorithm, we attempt to measure the length of a pseudo impulse response for the system described by $\mathcal{S}_2(x) - \mathcal{S}_1(x)$.
We assume a non-autoregressive, or finite-impulse-response behavior.
Because the system may be highly nonlinear, an observed impulse response
will be a function of both the state of the system at the time of application, and the magnitude (and slope) of the impulse.
In a high-dimensional setting, this creates the problem of having to
explore an infinite number of potential impulse responses all over the state space with varying impulses.
First, we create many paired stimuli for experimentation;
the individual stimuli of the pair are identical with the exception of the input sample at time k+1  where k is the maximal possible memory exhibited by either system.
%\reffig{pairedexps} illustrates the composition of a pair of stimuli for the memory experiment.  
Both systems are first excited with stimulus A and then both are excited with stimulus B.
Many such paired experiments are conducted, and the difference signals in response to stimulus B are observed at all nodes and are subtracted from those observed in response to stimulus A;
for each observation made in the range $(k+1,2k)$ the hypothesis 
tested (using the 2-sample Kolmogorov-Smirnov test)
whether the data in that time-step comes from the same distribution as the 
data observed before time $k+1$, when the systems were exposed to the 
perturbed input sample.

% \begin{figure}
% 	\centering
% 	\includegraphics[width=3.2in]{fig/inkscape/pairedExperiments}
% 	\caption{Example of differentially-designed paired experiments.}
% 	%\Description{A block diagram indicating the control-flow of the algorithm}
% 	\labelfig{pairedexps}
% \end{figure}




% Our task, however is not to compute the impulse response, but to estimate the duration of the response.
% And so, we begin by conducting a number of these tests and observing the
% responses (\reffig{pairedexps}).
% For every observation delay after the impulse, we see a
% range of magnitudes in response.
% The 2-Sample Kolmogorov Smirnov test is applied to all data at a given time delay, and the p-value is calculated for the hypothesis that that data comes from the same distribution as the data which preceded the impulse.
% The p-value is recorded for each hypothesized delay in the range
% (0,k).

More batches are simulated in a similar fashion,
each resulting in a length-$i$ vector of p-values for the hypotheses that observations at time-step $i$ are indistinguishable from unperturbed data.
After several batches are complete, we create a kernel density estimation for the distributions of p-values within each hypothesis.
As additional batches are calculated, the kernel density model is updated, and the Kullback-Leibler divergence is calculated between the old density model and the updated model.  
The algorithm terminates when the distributions of p-value for each hypothesis are well understood:

\begin{equation}
	\labeleqn{KLDconv}
    \frac{\text{KLD}_0}{\int\limits_i \text{KLD}_i} < \text{2\%}
\end{equation}

% \begin{figure}
% 	\centering
% 	\includegraphics[width=3.2in]{fig/inkscape/delayp-values}
% 	\caption{2-Sample K-S Test P-values for varying memory-
% 		length hypotheses}
% 	%\Description{A block diagram indicating the control-flow of the algorithm}
% 	\labelfig{kspvals}
% \end{figure}

% \begin{figure}
% 	\centering
% 	\includegraphics[width=3.2in]{fig/inkscape/kld-delay}
% 	\caption{Convergence of KL Divergence between successive updates of
% 		p-value density models}
% 	%\Description{A block diagram indicating the control-flow of the algorithm}
% 	\labelfig{kllearning}
% \end{figure}












% \subsubsection{Details of Memory Estimation Algorithm 2}
% This algorithm is presented as an alternative to Memory Estimation Algorithm 1.
% Rather than conducting many short trials, a single very long observation is 
% made, and it is analyzed under the alternative hypotheses of memory length 
% through a non-parametric statistical test for independence similar to the 
% Heller, Heller, Gorfine technique \cite{heller_consistent_2013}.   


% In the approach presented here, we simulate the systems in parallel many times with inputs of length $n$ drawn from the uniform distribution.
% For each hypothesized memory length, $m$, on the range
% $(0,k)$, we build a new data table from the corresponding subsets of $\bm{x}$ and $\bm{y}$.
% We then calculate the pairwise distances between all $\bm{x}$
% entries in the table, and sort the pairwise distances.
% We do not use conventional distance correlation, but first apply a linear weighting function to decrease the influence of long-distance observations \cite{szekelyMeasuringTestingDependence2007,UniquenessDistanceCovariance} .

% We reorder the responses ($\bm{y}_i \coloneqq { R_i }$) according to the sorting-indices of the X data computed in the prior
% step.
% What we then observe are the pairwise distances between response
% observations as a function of increasing pairwise $x$ distance
% (which are all unique under the assumption of memory length m).
% The rate of increase of the accumulated maximum distance in the response is an inverse indication of the likelihood that X and Y are independent.


% First, a memory length on the range $(0,k)$ is hypothesized, and observation data restructured accordingly (\reffig{datacomp}).
% Next, pairwise distances are calculated from the table of $x$ data (where the table composes $x$ according to a hypothesized memory length)
% using the Manhattan distance metric.
% The indices up to the median pairwise $x$ distance resulting from that sorting operation are then used to sort the corresponding Y data.
% Next, an accumulated maximum is taken along that 
% resorted $y$ data, resulting in data similar to that shown in 
% \reffig{entropic-edges}.
% Finally, the process is repeated for each hypothesized memory length on
% the range $(0,k)$.

% \begin{figure}
%   \centering
% 	\includegraphics[width=3.2in]{fig/plots/entropic_edges}
% 	\caption{Example of the accumulated maximum of Y
% 		distance with increasing X distance}
% 	%\Description{A block diagram indicating the control-flow of the algorithm}
% 	\labelfig{entropic-edges}
% \end{figure}


% Each trace of \reffig{entropic-edges} corresponds to the empirical increase in system response pairwise distances with increasing distance among inputs. We score each by simply counting the number of times its updated as the X distance increases.
% Those traces which rapidly rise likely correspond to underestimations of memory length.
% That is there is no apparent correlation between small distance in $x$ 
% and small distance in $y$.
% \reffig{hellerscores} illustrates the comparison of the scoring metric for two different system-pairs, the one in orange exhibits memory of 1, and that in blue exhibits a memory of 2.
% In order to increase the SNR of the prediction, for each hypothesis, an 
% alternate score is calculated which is equal to the score of the hypothesis 
% minus the standard deviation of the remaining scores.
% The index of the maximum score is used to predict the memory.

% \begin{figure}
%   \centering
% 	\includegraphics[width=3.2in]{fig/inkscape/hellerScores}
% 	\caption{Pairwise-distance-based memory hypothesis scores showing 
% 	pronounced peaks at the estimates corresponding to memory-lengths 1 (orange) and 2 (blue).}
% 	%\Description{A block diagram indicating the control-flow of the algorithm}
% 	\labelfig{hellerscores}
% \end{figure}







\subsection{Bootstrapping and Density Model Building}
With a good estimate of the memory order of the difference system, we begin 
repeating individual experiments of length $m+1$ using a uniform random stimulus as input.  
% This is equivalent to sampling the reachable state-space of the model 
% (assuming correct estimation of memory) in a uniform fashion.
During these experiments, we build a kernel density model of the output distributions that we observe from the paired systems, both 
common-mode: $\frac{1}{2}(\mathcal{S}_2(x) + \mathcal{S}_1(x))$, and differential mode: $\frac{1}{2}(\mathcal{S}_2(x) - \mathcal{S}_1(x))$.

Increased observability of internal nodes in the models adds dimensionality to this model, which includes all covariance terms.
We continue to make observations and update our model in batches, while measuring the Kullbeck-Leibler divergence between successive updates of the density models between batches.
The algorithm terminates with the K-L divergence between density models falls below a percentage of the accumulated sum of K-L divergence of all updates
in a fashion similar to \refeqn{KLDconv}.
In this way we ensure we have a minimally thorough sampling 
of both the state-space and the kinds of disagreement the models can 
produce.
  


%\begin{figure*}
%	\centering
%	\includegraphics[width=6in]{fig/plots/bootstrap2}
%	\caption{Density-models for error distributions between bootstrap batches.
%	Kullbeck-Leibler metric captures the informational value of the last batch
%	of bootstrapped experiments.}
%	%\Description{A block diagram indicating the control-flow of the algorithm}
%	\labelfig{bootstrap}
%\end{figure*}




\subsection{Tail Identification}
After having done a cursory uniform sampling of the differential system 
statistics, we have to identify 1) whether there is ``excess kurtosis'' in the
system and 2) the boundary of those regions which exhibit high-consequence 
and low-likelihood error.
In order to distinguish between the ``head'' regions and ``tail'' regions, 
we perform a piecewise linear fit to the sorted error of all bootstrapped 
observations.
%\reffig{tailid} shows the sorted vector and its fit.
As many segments as are necessary to keep the sum-squared error under 5\% 
are used.
The segment corresponding to the greatest amount of disagreement is used to 
represent the range of observations coming from the tail.


% \begin{figure}
% 	\centering
% 	\includegraphics[width=3in]{fig/inkscape/tail_ID}
% 	\caption{Depiction of piecewise-linear fit of sorted error data with the 
% 	identification of the tail segment.}
% 	%\Description{A block diagram indicating the control-flow of the algorithm}
% 	\labelfig{tailid}
% \end{figure}







\subsection{Resampling from the Tail in Transformed Space}
The existence of excess kurtosis and ``tail'' regions by definition implies that
our bootstrapping approach with uniform sampling will yield relatively little 
information from these regions.  It is also true that these regions are also 
those of the most interest to us in the task of validation. This section 
explains a strategy for efficiently and intelligently designing subsequent 
experiments so as to maximize the expected value of each and every simulation 
from this point forward.
The concept of Kriging is used, but is significantly adapted to 
accommodate the potentially very high-dimensional nature of our data in 
validation.  

First, we establish the desired number n of additional experiments to conduct 
per batch. Next, we define a neighborhood size, $b$. In this work, we used 
$ 3^(m+1) $.  Next, we sort the observations (in the tail) by decreasing 
discrepancy, and operate over the first n data points.  For each data point, we 
identify b data points which are adjacent in the state-space.  Next, we perform 
a PCA decomposition of that region of the state-space, centered at data point 
i, and keep only the first 2 components.  We then perform traditional Kriging 
inside the reduced-dimensionality PCA transformed neighborhood of the state 
space surrounding our point of interest.

Repeating this process n times yields n points in the region of the tail which 
should be simulated in order to yield additional information about the dynamics 
of the discrepancy in that region.

% \reffig{hdkriging} shows a portion of state-space where a high-density of 
% samples are 
% coming from the adapted Kriging approach.


%\begin{figure}
%	\centering
%	\includegraphics[width=3.2in]{fig/plots/hypersampling3d-3}
%	\caption{State-space resampling using neighborhood variance rebalancing.}
%	%\Description{A block diagram indicating the control-flow of the algorithm}
%	\labelfig{hdkriging}
%\end{figure}




\subsection{Model Augmentation from Tail Data}
At this point, we have a sufficient volume of information coming from the 
regions which are both the most interesting and the most challenging to 
excite.  Using this information, we train a machine-learning regression model 
to translate the output of System 2 into that of System 1, thereby completing 
the goal of identifying and learning from model deficiencies. In this work we 
have used an SVM regressor to perform a correction of the output of System 2.  

Once the regressor is trained, bootstrapping begins again, as the resulting 
model is now fundamentally different, and the statistics have to be re-explored 
from scratch.  There are two alternative termination conditions:  1) The 
resulting corrected model is now approximately normal and homoscedastic and no 
more exploration is required; at this point diagnosis can begin to move 
corrective structures and parameters into the interior of the model.  
Alternatively, 2) The chosen ML regressor is unable to learn high-complexity 
relationships; continued learning will be limited and we must revisit 
assumptions of memory-length and of ML complexity.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%   the remainder of this file is copypasta
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{comment}



\end{comment}
