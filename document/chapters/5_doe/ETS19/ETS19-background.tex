% !TeX root = ETS19-MAIN.tex

\section{Background}

Contemporary system designers find themselves spending increasing 
quantities of resources on design debugging, functional verification, and 
post-silicon validation \cite{keshava_post-silicon_2010} and \cite{_prologue_2016}.
While the analog specifications of modules at two different levels of the design
can be checked for equivalence, such a check does not guarantee behavioral equivalence across
the entire space of input stimuli because the set of specifications used to check for
equivalence may themselves be \textit{incomplete} 
\cite{deorioMachineLearningbasedAnomaly2013, 
guChallengesPostsiliconValidation2012}.
For similar reasons, formal methods for design equivalence checking may fail because designer inserted assertions may not cover all input-output behaviors and are generally myopic and constrained to specific input conditions. 

The use of rapidly exploring random trees (RRT) for analog circuit test 
generation was presented in \cite{ahmadyan_goal-oriented_2012, 
ahmadyan2013runtime}.
The idea was to quickly explore all reachable points in the state space of the analog circuit. Our approach, in contrast, progressively focuses on those points in the state space of the 
analog circuit where differences exist between the behavioral description of a circuit and its netlist
(or silicon) level implementation. 
The work of \cite{hu2018hfmv, yin2012verifying, lin_verification_2013}  was 
among the first to combine formal verification methods with simulation driven 
techniques to explore the limits to which analog design specifications can be 
stressed under multi-parameter circuit level perturbations without causing 
specification violations. Our objective in the present research
is not limited to device specifications but is applicable across the entire space of possible input stimuli to the system. 

There has also been past work on test generation driven analog design validation \cite{muldrey_ravage:_2013,muldreyDELOCDesignValidation2016, muldreyMixedSignalDesign2019}. The approach of \cite{muldrey_ravage:_2013,muldreyDELOCDesignValidation2016} focused on finding stimulus that maximized the
difference between two representations of the same system (e.g. behavioral vs. netlist),
learning and correcting for the difference using model-augmented learning kernels and repeating the process until no further differences are excited via stimulus generation. This approach incurs \textit{large numbers of learning iterations} for 
hierarchical system descriptions in which
differences exist between the two design representations above, across wide domains of the
behavioral space (\textit{even due to a single design bug}). The approach of
\cite{muldreyMixedSignalDesign2019} uses reinforcement learning algorithms to excite and learn about response
tail behaviors to the time and frequency domain statistics of input stimulus. This converges
to globally optimum solutions after multiple learning iterations as opposed to the genetic
stimulus generation approach of \cite{muldreyDELOCDesignValidation2016}, but is very slow, running into days of
computation on a modern personal computer for simple circuits such as amplifiers.


\section{Relevance to Prior Research and Key Contributions}

Prior design validation algorithms (genetic evolution based \cite {muldreyDELOCDesignValidation2016}, reinforcement learning based \cite {muldreyMixedSignalDesign2019}) used complex compute-intensive algorithms 
to design time-domain stimulus to emphasize temporal and statistical differences between the response of the designs at two different levels of design abstraction to the stimulus. These differences were used to train learning kernels designed to augment the high level  model and compensate for the differences observed. The process of test stimulus generation and behavior compensation was repeated iteratively (called learning iterations) until no further differences above, could be excited. 


\textit{Key Contributions:} As opposed to prior algorithms, instead of optimizing a single test stimulus for each iteration of
kernel based learning, a \textit {population of stimuli is evolved between each learning iteration} (note this is different from genetic evolution \cite {muldreyDELOCDesignValidation2016} in which a single optimal stimulus is produced from a population of stimuli in each learning iteration). This significantly reduces the burden of test stimulus optimization in
\cite{muldreyDELOCDesignValidation2016, muldreyMixedSignalDesign2019}, while allowing the learning kernels to learn
across the statistical characteristics of temporal and frequency domain device responses to an \textit {entire population of stimuli}
rather than a single stimulus in each learning iteration. Note that the majority of circuit simulation runs in
the test optimization steps of \cite{muldreyDELOCDesignValidation2016, muldreyMixedSignalDesign2019} are wasted (i.e. they do not
contribute to behavior compensation by the learning kernels in each learning iteration). In the current
approach, every circuit simulation run counts towards the overall behavior difference learning process.

Repeated observations of the dynamics of a pair of systems stimulated in unison (high vs. low level
circuit description) are made across the population of stimulus considered. A statistical model
describing their disagreement is constructed. 
The tails of the disagreement distribution represent high-risk, low-occurrence corners of the system's
operational space. Our approach rapidly identifies the tail-regions of this distribution and 
generates test stimuli whose simulations are likely to reveal valuable new disagreement data,
via simulation, 
coming from the highest-risk regions of the tail.
The population of test stimulus is evolved to include the generated stimuli and used to train the learning kernels as before. As further learning iterations occur, the tails of successive
disagreement distributions are eliminated until they become normal distributions with virtually
zero variance. \textit {This results in significant validation time speedup while maintaining high
coverage of modeling inaccuracies and design bugs}.



