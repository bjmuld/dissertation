% !TeX root = ../../muldrey_thesis.tex


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%   Chapter: Key Concepts
\section{The Relative Merits of Stimuli}
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




It is the job of a stimulus to reveal information about the system, a ``good''
stimulus revealing a large amount of information.
What we see when we look at a system's response is only a transformed version
of the same information present in the stimulus.
And so, a high-complexity stimulus doesn't necessarily hold any advantage over 
a simple
one.

Let us look at a simple example.
\reffig{sigs_summary} depicts three different stimuli within the range (-1,1)
which are applied to a (memoryless) function.
Comparing the stimuli (in the first row),
we see that the sinusoid is highly structured, but lacks complexity.
In contrast, both the Gaussian and uniform white noise lack structure but are 
rich in 
complexity.
In the second row, we see the system's response to each stimulus; we note that 
the
range of data expressed in all cases in comparable.
In the third row, we compare the shapes of the data distributions in the 
stimuli and
in the responses.
One measure of the system is the degree to which information is transformed by 
passing
through \cite{kullbackInformationSufficiency1951}.
In the third row, we can see the underlying function emerge in a plot of output 
vs. input.
Shaded vertical bars correspond to increased observational density in the input 
space, and 
shaded horizontal bars correspond to increased observational density in the 
output space.

If we were to attempt to model the function, which would be our preferred method
by which to sample the function?
The sinusoidal input results in a large
number of samples coming from the extremes of the function's range
and very few samples from its interior.
The normally distributed noise, however, results in heavy sampling
from the portion of the function's range near the origin, where it is mostly 
linear.
Being approximately linear, there is little transformation of information in 
this region,
explaining why the distribution of output data so closely matches that of input 
data.
The uniformly distributed noise, generates samples coming chiefly from the 
extremes,
though it's not as imbalanced as either the sinusoid or the Gaussian noise.

In this case, since we've discovered that most of the function's curvature lies
in those regions most heavily sampled by the uniform white noise,
we would prefer it.
To use the Gaussian noise to glean the same amount of information revealed by 
the
uniform white noise, one would have to conduct several experiments.
And so, the Gaussian white noise stimulus lacks economy in this case.
It should again be noted that we can only draw conclusions about efficiency 
after
establishing a basic understanding of the location of features in the function 
space.


\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/concepts/all_one_page}
    \caption{Comparative Study of Stimuli for a Memoryless System:
        Observations of informational content revealed by three stimuli, a 
        sinusoid,
        Gaussian white noise, and uniform white noise.
    }
    \labelfig{sigs_summary}
\end{figure}


Let us now look at a more complex example.
\reffig{sigs_summary_mem} summarizes experimentation on a system which has 
memory.
In the first row of plots we see our familiar input stimuli decomposed into
sets of two-dimensional input data.
We note that the sinusoidal stimulus only covers a thin slice of the input space
parameterized as $X \coloneqq \{ x(t),; x(t-\tau)\} $.
In the second row of plots, we see the responses of the system to the different 
stimuli.
Here we note the diversity in response amplitudes.
For a better understanding of why these responses look so different from those 
in the
memoryless case, we turn to frequency domain representations of the data.
In the third row of \reffig{sigs_summary_mem}, we can compare the distributions
of energy across frequency for all stimuli and their corresponding responses.
One should note that in a memory-having system modeled under an assumption of 
memoryless-ness, the output does not appear to be a function of the input 
(strictly speaking).
One also observes that the different input distributions result in 
different
sample densities in the input space and output space.
We see the system nonlinearity manifested in the presence of additional 
high-frequency
energy relatively speaking.

The fourth, fifth, and sixth rows all present the results of solving a
\gls{rls} problem with our data under varying 
assumptions of 
input dimension.
In the fourth row, we model the system as memoryless,
and we note that no experimental data is sufficient to train a successful model.
In the fifth row, we model the system as having memory = 1,
and we find that the RLS model trained on sinusoidal data can provide reasonable
prediction accuracy, but the data resulting from Gaussian and uniform noise
stimuli is insufficient.
It is not until we reach a model complexity with a memory of 64 data points, in 
the
sixth row, that the data from the Gaussian and uniform stimuli can be put to 
use.

Hopefully these examples have brought to light some of the dimensions of the 
stimulus
design space and some of the fundamental trade-offs.
The principal considerations are:
\begin{enumerate}
    \item The time duration of the stimulus: though constant in our examples,
    a longer test can potentially reveal more information than a shorter one.
    \item The complexity of the stimulus: This correlates with the difficulty
    of interpreting results.
    \item Stimulus coverage of input space: if we measure the domain of our test
    in the input space, we must consider how our stimulus covers that space.
    \item Sample distribution in output space: test  economy depends on how many
    repeated measurements are made.
\end{enumerate}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/concepts/all_one_page_mem}
    \caption{Comparative Study of a System with Memory:
        Observations of informational
        content in experimental data using sinusoidal, Gaussian white noise, and
        uniform white noise.	
    }
    \labelfig{sigs_summary_mem}
\end{figure}