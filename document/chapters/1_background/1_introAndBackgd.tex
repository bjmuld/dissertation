% !TeX encoding = UTF-8
% !TeX root = ../../../muldrey_thesis.tex



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%    Intro and Background
%%
\section{Circuit Design Methodology}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


As contemporary \gls{soc} designers push for ever more functionality 
and performance from their designs, they are finding that additional design 
complexity and bleeding-edge process nodes are leading to an increase in 
\gls{post-silicon} activity in the design-phase (verification, testing, and 
diagnosis) with most of it occurring around \gls{ams} components and their 
integration.
Designs are undergoing increasing numbers of silicon re-spins due to problems 
originating in both design and manufacturing errors.
Troubleshooting between respins yields varying degrees of information to be 
fed-back to designers and process engineers 
\cite{keshava_post-silicon_2010, gielenComputerAidedDesignAnalog2000}.



%For example, slew-rate and gain-bandwidth-product are common performance
%specifications for operational amplifiers. Several input waveforms or waveform
%sets are commonly used to estimate these specifications; one contemporary
%challenge is applying these tests to operational amplifiers embedded within
%larger systems and whose inputs are obfuscated by other nonlinear systems.
%In 1985, Bandler et al. provided a comprehensive review of diagnosing
%faults embedded in side a network, but they assumed linear elements.
%The landscape and the role of AMS designers has changed dramatically since
%1985, principally in the level of integration with and within other (nonlinear)
%systems.
%Historically, the majority of analog designs existed as monolithic silicon
%with its own packaging, its own test-points, etc. and the principal designer
%exercised total control over all aspects of the design.
%However, today's AMS designers must
%integrate their products into much larger and more complex systems on the same
%die while retaining a degree of testability.

\begin{figure}
	\centering
	\includegraphics[width=4in]{figures/ITC16/dtle_io-eps-converted-to}
	\caption{A Modern Fault Localization Problem visualized in a 
		high-speed I/O Discrete-time Linear Equalizer \cite{ismail_8gbps_2015}}
	\label{dtle_fig}
\end{figure}

\noindent
In current practice, \gls{ams} testing and debugging techniques tend to be 
ad-hoc and so result in long lead-times, high human effort, and low tool-reuse.
Additionally, successful industry practices are heavily guarded
intellectual property, and are not routinely disclosed within the community.

Traditional \gls{test_stimulus} design is predicated upon 
reliable knowledge of both the design and manufacture of the \gls{dut} as well 
as presumption of realistic \gls{fault} models and/or \gls{bug} manifestations.
In cases where the design is thoroughly known and accurate models of 
all behaviors exist, hierarchical simulation models can be used to 
probabilistically target failure modes (or process variation or bugs) and 
generate tests which might best probe those vulnerabilities and carry 
information to an observable output.

At present, there exist varieties of contexts in which either the
abstraction \gls{model}, composite model, fabricated \gls{dut}, or 
some combination cannot be relied upon to be accurate or dynamically 
consistent representations of the same thing.
Disagreement between \gls{dut} and model or high- and low-level models
can arise as a result of
process variation, design tool limitations, or model shortcomings.

Further complicating matters, most modern test paradigms rely on a means of
test pattern generation which itself requires a trustworthy model.
In situations of \gls{post-silicon} validation, the device's simulation model 
cannot be relied upon as a vehicle for test design because physical realization 
of the device may introduce behaviors which the model does not include (e.g. 
ground bounce, negative-bias temperature instability, positive-bias 
temperature instability, radiant leakage, cross-module coupling, power supply 
limitations, etc.).

Additionally, in large \gls{ams} designs, observation and control of internal signals
and states is rarely complete;
this is the case in silicon and similar conditions can arise in simulation due to practical
constraints on data collection.
Even the most novel \gls{dft} techniques cannot provide access to all internal
nodes of an AMS chip (Figure \ref{dtle_fig}).
The inability to directly stimulate and observe embedded module inputs
and outputs is a tremendous impediment to detecting undesired system behavior
and identifying the origin of design or manufacture errors.

What's required is a comprehensive framework for establishing and quantifying trust
at low levels of the model hierarchy which can be measured and propagated across
abstraction layers and through composite simulations.

%In the case of hardware anomaly detection, the inverse is true: the simulation 
%model of the designer might be accurate enough, but the fabrication of the 
%\gls{dut} 
%might not be trustworthy.

%In order to perform either abstraction-model validation or post-silicon validation,
%one must measure the degree of equivalence between a single \gls{dut} and a single model.
%Similarly, pre-silicon validation requires quantification of equivalence 
%between different design-descriptions of a component.
%Finally, debug/diagnosis requires estimating relative probabilities of 
%equivalence between individual sub-components in hardware and their models.

\begin{figure}
	\centering
	\label{postSciProblem_fig}
	\includegraphics[width=\textwidth]{figures/postSiProblem_vert.pdf}
	\caption{The Contemporary Post-Silicon Validation Problem As Analogy}
\end{figure}
