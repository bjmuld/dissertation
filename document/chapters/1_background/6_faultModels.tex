% !TeX encoding = UTF-8
% !TeX root = ../../muldrey_thesis.tex
% !TeX TS-program = pdflatex
% !BIB TS-program = biber
% !TeX spellcheck = en_US


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%% 
%%   Post-Silicon Testing Section
%%
\section{Fault Modeling}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





One element of the formal study of testing which applies to 
modern testing problems is the fault model.
%If one proposes to detect or diagnose a fault, he must first define ``fault.''
A classical example from digital testing is the ``stuck-at'' fault: a fault 
model proposed by Armstrong which generalizes all manner of physical defect 
within a gate by fixing a faulty gate model's output at one or 
the other logical value \cite{armstrong_finding_1966}.
The stuck-at model enabled a volume of work; some which revealed that it's  
possible for a fault to exist yet have it's presence completely 
masked \cite{goel_implicit_1981}.
This begs the question of what is means for a \textit{system} to be faulty 
because though it may contain a fault, a system's behavior may be 
indistinguishable from a fault-free system.
Sunter addresses this in \cite{sunter_practical_2014} for \gls{ams} systems by 
defining a faulty system as one whose performance violates at least one 
system-level specification.

Commonly assumed transistor fault models include gate-to-drain, 
gate-to-source, and drain-to-source short-circuit defects as well as gate, 
drain, and source open-circuit defects. 
Fabricated analog circuits pose a more nuanced problem: everything exists on a 
continuum, including opens and shorts.
A traditional distinction drawn in papers on \gls{manufacturing_test} is that 
between 
``hard'' and ``soft'' faults.
Hard faults are those that model catastrophic device failures while
soft faults are those arising due to variation of at least one
parameter to sufficient degree \cite{bandler_fault_1985, 
kabisatpathy_pseudo-random_2002}.
A tremendous amount of work has been done which addresses these two categories 
independently.
For example, Milor and Zhang go after catastrophic failures 
using alternate tests and 
statistical techniques \cite{milor_detection_1989,zhang_virtual_2011}
%	huynh_automatic_1999,korzybski_dictionary_2008},
while others have focused on the detection of parametric faults
\cite{nagi_fault-based_1993, guo_observer-based_2002, hu_soft_2007, 
	stratigopoulos_efficient_2014, yoon_fault_1998, 
	kabisatpathy_pseudo-random_2002,slamani_testing_1992}.
Sunter and Stratigopoulos have proposed techniques for deriving coverage 
metrics for tests addressing faults of both kind in \cite{sunter_test_1999} and 
\cite{stratigopoulos_efficient_2014}. When dealing with nonlinear circuits, 
it's not clear that good coverage of ``hard'' and ``soft'' faults translates to 
coverage of ``medium'' faults.

To distinguish between hard- and soft-faults belies an underlying commonality:
a hard fault is but an extreme degree of a soft fault.
Kundert, Mitra and others acknowledge that there exists a larger category 
beyond ``hard'' and ``soft'' in which one would find systems which fail due to 
crosstalk between components, power supply bounce, clock feed-through, 
oscillator lock-in and pull, or radiation 
\cite{kundert_verification_2006,mitra_post-silicon_2010,
	keshava_post-silicon_2010,liu_towards_2011,singerman_transaction_2011},
thus reformulating the classification of failures into two categories: 
``parametric faults'' and ``everything else.''
In contemporary validation, detection, and diagnosis testing, the ``everything 
else'' category requires equal attention, though the current body of knowledge 
doesn't yet explain how to provide it.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%% 
%%   Post-Silicon Testing Section
%%
\section{Dynamics Modeling}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


In this work, we are primarily dealing with problems existing between a 
detailed model and an incorrectly specified reduced-order abstraction model.
A great deal of focus is given to the analysis and decompilation of system 
dynamics in effort to build and/or refine hypothetical reduced order models 
from observed dynamics -- given the defficient abstraction model.

There has been a great deal of work in the domains of \gls{mor} and automated 
model extraction.
For example, work in 
\cite{vasilyevTBRbasedTrajectoryPiecewiseLinear,
    rewienskiTrajectoryPiecewiselinearApproach2003,
    pengliCompactReducedorderModeling2005,
    roychowdhuryReducedorderModelingTimevarying1999,
    bondStableReducedModels2009}
all propose tools for extracting from an explicit set of differential equations 
a reduced-order approximate representation of the same system in a 
lower-dimensional phase-space.
Generally, Krylov subspace methods are employed and the techniques are applied 
to linear or \textit{weakly nonlinear} systems through various piecewise 
assemblages of linear models.

In \cite{chenjieguModelReductionProjection2008}, the authors present an 
extension of the projection of the ODE system by performing non-linear 
projections onto manifolds in high-dimensions as a way to further reduce 
redundancy observed in linear projections of non-linear systems.

In \cite{borchersEquationbasedBehavioralModel1996}, the authors present a more 
heuristic, circuit-designers approach for accomplishing the same goal.
In that work, the authors were able to achieve a reduction in the number of 
model variables on the order of 50 percent, and a reduction in the number of 
model parameters on the order of 1000 percent, leading to simulation-time 
speedup of on the order of 10x.

Similarly, the authors of \cite{nathkeHierarchicalAutomaticBehavioral2004} 
systematically null equation elements contributing to each row of the system's 
ODE, thereby removing low-consequence dynamical factors.

Other researchers have proposed abstraction models which step outside of the 
numerical ODE/PDE simulation paradigm.
For example, in \cite{karthik_abcd-nl:_2014}, the authors represent 
trajectories through a system's state-space in the form of entries in lookup 
tables and re-structure analog dynamics as a series of sequention table 
look-ups, thus enabling digital verification techniques to be applied to the 
booleanized circuit.
Additionally, the authors of \cite{bondCompactModelingNonlinear2010}, in order 
to preserve the stability of the system, build-up from scratch a new 
polynomial-basis ODE representation of the system, solving for parameters which 
meet accuracy criterion as they go.

Also of note: a patent was issued in 2002 in which the authors present a 
modeling technique which resides entirely in the context of an FPGA wherein 
individual executables are (optinally dynamically compiled) and dynamically 
linked by a solver depending on the state of the simulation.
In this way it functions as a dynamical piecewise (non-) linear simulation code 
execution environment.

After synthesizing a new reduced-order model, they must be checked.
Nearly all existing literature or MOR uses a fixed set of a priori identified 
randomly chosen stimuli, exhaustive enumeration of discretized stimuli, or 
fixed sets of \textit{likely} stimuli to evaluate the performance of the model.
The measure of perforance is almost always \gls{mse}.
The sortcomings of \gls{mse} as a performance metric in this context are 
addressed in \refchap{doe}.
Some literature, such as \cite{wangAnalogCircuitVerification2011}, address 
model verification in a more dynamic way.

In \cite{wangAnalogCircuitVerification2011}, authors use a meta-language known 
as "Bounded Linear Temporal Logic" which operates on extracted properties of 
time-domain observations to project them into a boolean space.
This logic provides a way to formalize performance properties like ``gain'' 
over arbitrary trajectories in a system's state-space and to bound acceptible 
ranges to be considered ``valid.''
The authors then employ a Baysian sampling scheme to run simulations and 
collect evidence on whether properties are consistent between models.
