% !TeX encoding = UTF-8
% !TeX root = ../../muldrey_thesis.tex
% !TeX TS-program = pdflatex
% !BIB TS-program = biber
% !TeX spellcheck = en_US




\subsection{Defect-based Testing}

Defect-based testing holds the allure of quick, directed tests that target 
commonly observed defect mechanisms, forsaking rigor for speed. 
It requires an enumerated dictionary of failure modes at the beginning; then it 
creates a minimal test sensitive to a maximal portion of the dictionary 
\cite{sunter_practical_2014}.

In \cite{aminian_neural-network_2000}, the authors use an impulsive stimulus to 
excite various faulty filters. After training, an \gls{ann} classifies systems 
as ``pass'' or ``fail.''
Methods were introduced by \cite{slamani_multifrequency_1995} wherein the 
authors explore circuit nodes most sensitive to a fault set and find a 
multi-tone stimulus that can be used to increase net detectability of the fault 
set.

\subsection{Diagnosis}

Diagnosis, like defect-based testing, is concerned with local phenomena on the 
die. Though defect-based testing merely detects the presence of any single 
fault, diagnosis attempts to infer \textit{which} fault is present.
Figure \ref{technique_overview_figure} shows the general classes of 
methodologies as presented by \cite{bandler_fault_1985}.

\begin{figure}[!h]
	\centering
	\includegraphics[width=5in]{figures/ITC16/technique_overview-eps-converted-to}
	\caption{The Fault Location Technique Universe \cite{bandler_fault_1985}}
	\label{technique_overview_figure}
\end{figure}

The bulk of work in the field of failure diagnosis can be 
found in either of two categories of literature: those that frame fault 
localization as a ``gray-box'' system identification problem, and those which 
frame it as a classification problem.


%% analytical methods:
\subsection{Analytical methods} (stringent model requirements)

\noindent
Analytical methods attempt to diagnose the location of faults through formal 
mathematical approaches, typically by solving optimization problems to calculate
parameter values.
They require ``well behaved'' and accurate component models.

In \cite{tadeusiewicz_algorithm_2002}, the authors detect and diagnose 
relatively small deviations in component values of linearized circuits 
using linear programming methods to evaluate the feasibility of 
single-parameter deviations leading to observed behavior.
In \cite {cherubal_test_2001}, the authors present a methodology for diagnosing 
specification violations by through alternate test inferences of parameter 
values in analog/mixed-signal circuits.
Similarly, techniques such as those presented in 
\cite{tadeusiewicz_algorithm_2002} and \cite{sen_fault_1979, 
slamani_analog_1992, banerjee_automatic_2015, jeong_robust_2015, 
erdogan_detailed_2010} leverage information from observed outputs to 
back-calculate model parameter values when the model is known to be accurate.

%% classification methods:
\needspace{3\baselineskip}
\subsection{Classification Methods} (require fault dictionary)

\noindent
Most modern contributions employ machine-learning algorithms to classify failed 
systems based on the similarity of observed behaviors to a set of prototypical 
faulty behaviors.
The inherent shortcoming of these approaches is the necessity of a 
training set of failed systems for the classifier to use.
Arrival at a training set for a large contemporary design, however, is no easy 
task.
One must enumerate a set of ``feasible'' faults which, for systems of even 
modest size, will result in intractable numbers.
And so commonly, one must then intelligently sample the relevant design and 
fault space to reduce the 
number 
of simulations required to a reasonable size \cite{sunter_practical_2014, 
	stratigopoulos_efficient_2014, stratigopoulos_fast_2014}.

\cite{aminian_neural-network_2000} uses a neural net classifier to diagnose 
faults based on systems' impulse response rather than an optimized stimulus.
Similarly, fuzzy classifiers \cite{catelani_soft_2002}, white noise stimulus 
\cite{spina_linear_1997}, and multi-tone stimulus 
\cite{abu_el-yazeed_integrated_2003} can be used to perform fault diagnosis 
provided the 
fault in question is represented in the training set of faulty devices, 
something very difficult to guarantee.
In \cite {chakrabarti_fault_1999}, a methodology for sampling from a fault 
universe (parametric faults) is presented which leverages the fact that many 
faults have similar ``syndromes'' which can be clustered.
%
%Here, regression analysis requires a set of \glspl{dut} with which to train 
%the 
%regression function, so even though its not strictly model-dependent, it 
%requires a set of known ``good'' \glspl{dut} and a set of known ``bad'' 
%\glspl{dut}.
%Such sets aren't available either for post-silicon validation or potentially 
%for hardware trojan detection. 
%
\cite{cherubal_test_2001} and \cite{variyam_prediction_2002} use 
a genetic algorithm to optimize a piecewise linear test stimulus before using 
regression modeling to relate the device parameters to 
the measurements made on the \gls{dut}. It was shown that from this 
relationship, a 
cause-and-effect analysis could be used to determine the parameter whose 
variation led to the system specification violation.
%One might employ a digital testing model and identify faults and create 
%pass/fail criteria based upon process defects such as shorts, sizing issues, 
%etc. \cite{walker_vlasic:_1986}.
In lieu of an analytical identification of the faulty 
system, \cite{muldrey_-LOC:_2016} presents a heuristic best-guess approximation 
of the buggy subsystem based on feasibility estimation.