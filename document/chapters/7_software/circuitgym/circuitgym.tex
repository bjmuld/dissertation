% !TeX root = ../../../muldrey_thesis.tex


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                        	%
%                                           				%
%     CHAPTER:   SOFTWARE TOOL CREATION     				%
%                                           				%
%                                           				%
\section{Circuitgym: Extending OpenAI Baselines to Circuits}
\labelsec{circuitgym}										%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{Introduction}

Circuitgym is the name of a python library I created to meet the requirement
of creating a \gls{mdp}-style interface compatible with
the OpenAI ``Gym'' API for circuit simulations.
Such an interface was required for conducting reinforcement learning
experiments in a circuit environment.


\subsection{Motivation}

In the course of my research, the question arose whether there were better means
of re-using simulation results carried out by previous optimization routines.
Reinforcement learning showed promise in this regard; at any point in time, 
an AI actor could be trained using a collected history of data.
Initial experimentation was done by implementing a few popular RL algorithms in
Matlab and experimenting on systems of Volterra filters (\refchap{rl}).
I found that my algorithms were not parametrically robust, and so I turned to
the more rigorously tested implementations found in the open-source library,
``Baselines,'' released by the OpenAI research company \cite{dhariwalprafullaOpenAIBaselines2017}.
The reparameterizations of the algorithms as implemented by Baselines
proved easier to use and more stable than my own crude Matlab implementations.
It was when I sought to replace the Volterra filter models with 
spice circuit simulations that I realized an additional tool was needed
to form a layer between the spice simulation back-end and the reinforcement learning
agent.


\subsection{Design Considerations}

Many of the major reinforcement learning libraries, including not only OpenAI Baselines
but also Keras-RL \cite{plappert2016kerasrl} and Stable-Baselines \cite{stable-baselines},
interact with the underlying environment via the same API, defined by Baselines.
The API is fairly simple: the environment must respond to a ``reset'' command, it
must implement a quantified ``action space'' from which an agent can select
actions, and after every action, it should return a vector of new observations
reflecting changes in the environment transpiring as a result of the previous action.

With the API spelled out, I needed only to wrap a spice simulation in a way that
a sequence of discrete actions could be taken, with the spice simulation progressing
in the background in such a way that agent actions were accounted for in the simulation.
Because latter portions of the simulation required input and state information which are
not yet known, simulations cannot be carried out in their entirety as single 
spice analyses.
Instead, an individual spice simulation would have to be run for each step of the reinforcement
learning ``episode.''
For this reason, Pyspectre was developed \refsec{pyspectre}.
But additionally, Circuitgym required standardization of the circuit input mechanism,
a way to avoid simulation discontinuities between adjacent simulations, and 
a configurable way to return information about the intervening simulation.

