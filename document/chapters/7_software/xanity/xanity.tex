% !TeX root = ../../../muldrey_thesis.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%555
%%
%%
%%     Appendix: Software Tools
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\section{Xanity: An Experiment Runner and Data Management Tool}

\subsection{Introduction}
``Xanity'' is the name of a software tool which came gradually into existence 
over the course of my research.
Every time I'd embark on a new course of experimentation, I would bring 
certain pieces of code with me, in order to facilitate the logistical side of 
the work: source code snap-shotting and version control, notes on individual 
trials, dependencies on 
external libraries, etc.
Eventually, Xanity was given a name and became a project in its own right.

\subsection{Motivation}
The two central complications and nuisances in my research which Xanity 
addresses arise from the use of virtual environments for 
individual lines of experimentation and from the need for book-keeping in 
the volumes of data produced during debugging and bona fide experimental runs.

Virtual environments created by tools like ``virtualenv,'' ``venv,'' 
``pipenv,'' and Anaconda are very useful and popular for managing the webs of 
dependencies created when using mixtures of external tools.  
Individual circumstances have particular version requirements and system 
library requirements; additionally, portability and 
collaboration require delegating and codifying the task of managing code 
environments.
As a result, rather than managing libraries, experimentalists now have the task 
of managing environments.
Any individual experiment must be run from inside the correct environment, and 
the constant context switching can become cumbersome.

Additionally, experiments generate data; both
in bona fide experimentation and during experiment debugging as well.
Some data has a global scope and is recalled by many experiments, and other data
is regenerated routinely with every run.
Some data is computationally costly, and should be reused, even if only for 
debugging purposes, and other data is cheap, kept only for documentation or 
record-keeping.
Without a book-keeping mechanism, keeping track of the various kinds and pieces 
of data 
can be quite burdensome, tedious, and costly in terms of disk space.

The major design goals of Xanity were as follows:

\begin{enumerate}
    \item \textbf{Collocation of Experiment Collateral} In order for projects 
    to be 
    more portable (both within one system and across systems), all dependencies 
    in terms of source code, environment, and data should all be collocated.
    Should an experimentalist have to move project directories across file 
    systems, the integrity and usability of the experimental setup should not 
    be compromised.

    \item \textbf{Clear Cataloging of Data} To mitigate the burden of keeping 
    track of various ad-hoc data-storage regimes, Xanity should implement a 
    self-explanatory and generalizable means of organizing and storing data.
    
    \item \textbf{Simplification of Setup} The process of getting a new system 
    ready 
    for experimentation can be daunting. Both Anaconda and Pip have mechanisms 
    for snap-shotting the states of environments which enable easy replication.
    Use of these mechanisms often is not straightforward and requires both the
    collaborator and collaborate-ee to be familiar with the advanced usage of 
    the tools.
    
    \item \textbf{Repeatability} Critical to the progression of exploration 
    itself, repeatability should extend from copying a project or base of code 
    all the way through production of the concluding analysis.
    The complexity in modern tool chains and setups represents tremendous 
    variability which, left un-addressed, can compromise the repeatability of 
    experiments.   
    
\end{enumerate}

\subsection{Implementation}

Central to the implementation of Xanity is the desire to leverage existing 
tools from the software development world like Git revision control tools and  
Anaconda and Pip package managers, but in such a way that a scientific 
experimentalist could benefit from them without having to become expert in 
their use.

For environment management, Xanity can delegate environment creation and 
entrance/exit to Anaconda and Pip.
Xanity must, however, exist both inside and outside the experimental 
environments.
For running experiments, execution control must initiate in the system 
environment where Xanity can 
orient itself, parse user requests, and then ``step into'' the appropriate 
environment.
It can then pass control off to the Xanity instance inside.
In order to do this, Xanity must make sure that Xanity itself is installed 
whenever it creates new environments.
A repeatability problem arises, however if Pip is used; there could be a change 
in Xanity version between Xanity installation at the system level and Xanity 
installation during environment creation.
For this reason, Xanity is designed to be independently 
\textit{self-replicating}.
During the initial installation at the system level, Xanity creates a symbolic 
copy of itself as an installable python package which it will use to install 
Xanity into experiment environments.

While there are databases and tools abounding for data management,
none are quite as portable, transparent, and user-friendly as the file system 
itself.
Use of the file system enables other tools like Git and other human users to 
easily parse and keep track of data in a familiar way.
And so, Xanity implements a data directory structure that is easy to 
understand and easy for Xanity to parse and recall experimental data.

For recalling data, Xanity implements a data-query API in which all matching 
data can be easily recalled by any experiment or analysis in a Pandas DataFrame 
object
containing experiment metadata for each match.
A limited number of data management tools have been implemented as well.
Xanity has components for summarizing the data associated with a project and 
associated with individual experiments within.
Xanity also has mechanisms for scoring the ``value'' of data saved in an 
individual run so that empty and low-value run directories can be identified 
and pruned.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{figures/xanity}
    \labelfig{xanity}
    \caption{A graphical representation of the Xanity framework, illustrating 
    commandline entrypoints for interacting with a Xanity project.}
\end{figure}



