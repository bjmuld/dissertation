% !TeX root = ../../../muldrey_thesis.tex


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
%%
%%    Chapter: Software Tools
\section{Pyspectre: A Modern Python Interface to Cadence Spectre}
\labelsec{pyspectre}
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\subsection{Introduction}

Pyspectre is the name of a  tool I developed in the course of
this research
in order to provide a lightweight, low system-overhead python interface
to the Cadence Spectre\textsuperscript{\textcircled{R}} circuit simulator that would
allow multiple instances of Spectre to be held in memory,
manipulated, evaluated, and re-evaluated with minimal cpu, memory, or disk overhead.
Pyspectre utilizes an undocumented ``interactive mode'' of the Spectre binary
which implements a ``read, evaluate, print loop'' (REPL) and accepts a number of commands
through standard input.
Pyspectre carefully invokes, maintains, and interacts with a running instance of
Spectre inside a subprocess while extending access to other
objects in the python environment.
Pyspectre is available on the Python Packaging Index for installation; its source
code is available on Gitlab.com for use and contribution \cite{muldreyPyspectre}.


\subsection{Motivation}

%In the course of the optimization-centric portions of the work, the economy of
%running optimization after optimization was questioned.
%In such a fashion, any individual optimization routine is completely independent of
%those that have come before, and it is unable to reap any benefit from the
%(sometimes tens of thousands) of simulations that had been conducted in the course
%of earlier optimizations.
%Reinforcement learning held promise because the ``learned'' information is reflective
%of all previous simulation and is retained indefinitely.

Modern reinforcement learning algorithms operate on the assumption that their environment
can be framed as a Markov decision process.
Framed as such, RL agents take action on a periodic basis and they are trained in
the same manner.
And so, in a circuit simulation environment, this corresponds to periodically
pausing the simulation, passing the circuit's state to the RL agent,
asking the agent to infer what subsequent action should be taken,
and manipulating circuit inputs to reflect the RL agent's choice.

Cadence provides programmatic interface to Spectre through their ``Ocean'' tool
so that scripts can be written in their ``Skill'' language for automated simulation.
These mechanisms are not suitable for realtime, on-the-fly manipulation of a stimulus
over the course of a single simulation because the Ocean scripts are statically interpreted.
Additionally, Cadence provides a command-line interface for Spectre, but one would
be limited to creating individual netlists and running individual simulations for each
action taken by the RL agent. \reffig{spectresteps} illustrates that process:

\begin{figure}
	\centering
	\labelfig{spectresteps}
	\includegraphics[width=6.5in]{figures/multipleSpectreCalls}
	\caption{An illustration of how a reinforcement learning algorithm would interact with Cadence
		Spectre through Cadence's provided command-line interface.}
\end{figure}

Either of the available mechanisms seemed inadequate considering that a single
test stimulus would consist of hundreds of individual RL learner actions.
A great deal of time would be wasted having to create new Spectre processes, allocate
memory, and parse the (nearly identical) netlist each time.


\subsection{Design Considerations}

The primary design consideration for Pyspectre was speed.
In the course of a single training session, a reinforcement learning algorithm
may be exposed to tens of thousands of ``episodes'' (each episode corresponding to 
one stimulus). With each episode consisting of hundreds of RL actions,
it's quite feasible that on the order of millions of individual transient simulations
would be conducted during a training session.
Pyspectre sought to minimize the computational overhead for each evaluation.

\subsubsection{Parallelism}

A secondary consideration of Pyspectre was to enable multiple instances of Spectre to
be maintained simultaneously.
In order to do this, several default behaviors had to be modified to prevent 
Spectre instances to have write collisions on various files.

\subsubsection{Circuit-object Portability}
Additional utility in Pyspectre would come from bundling all the requisite simulation
collateral including netlists, stimulus files, model files, etc. into a serializeable
python object.
In this way, simulations could be easily sent across the network for evaluation on remote machines.

\subsubsection{Compiled Model Maintenance}
Early iterations of Pyspectre created temporary directories in the file system
within which to operate individual instances and maintain individual circuit state
files.
The arrangement overlooked the production of compiled AHDL models which by default were
created in these directories.
As a result, each instantiation would re-compile AHDL models that may have previously
been compiled.

\subsection{Implementation Details}

Pyspectre was implemented in the Python language, in a manner compatible with Python v2.7 and v3.x.
The ``pexpect'' library was used for its ``ReplWrapper'' class which spawned the spectre
process and provided a string interface for interaction.

\subsection{Libpsf}

Libpsf is the name of a legacy project by Henrik Johansson \cite{johanssonPSFSimulationData2019} which
implements a PSF file reader in the C/C++ language.
PSF is the native (proprietary) file format of the Cadence simulators, whose structure, as far
as I know, hasn't been released publicly.
I presume that Mr. Johansson has done the work of manually parsing and reverse engineering the
file format; in any event, he has made his code available for all on Github.

Unfortunately, Libpsf was not a turn-key Python installation like so many others. 
The Python bindings were well out of date, having been implemented in 2014 using a
flavor of the Boost::Python library which was built against legacy versions of the Numpy
numerical computation libary \cite{BoostLibraries} and \cite{NumPyNumPy}.
Libpsf would not compile with the contemporary releases of Numpy which are used by all
modern Python tools.

In order to extract data from Spectre in its native and compact binary format,
I would have to update the Libpsf codebase to interface with modern Numpy.
In order to achieve this, I followed the distribution recommendations of the Python
Packaging Authority, using their system images based on legacy CentOS releases to compile
a maximally compatible version of Libpsf.
The source code was modified for python 2.7/3.6/3.7 compatibility and was linked 
against Boost v1.68 and Numpy v1.15.
The compiled code was then packaged into ``wheels'' for each of the Python versions
and was uploaded for distribution on Pypi.org \cite{Libpsf}.

\subsection{Performance}
To measure performace, I used a netlist which includes a transistor-level low-dropout
DC regulator as well as a behavioral-level version of the same circuit.
To measure performance, I setup an experiment which will simulate a single reinforcement learner
action (a transient simulation from $0s$ to $1 \mu{}s$) and will take that same action 100 times
first using Spectre's command-line interface invocation, and 100 times again using the pyspectre
interface.
We expect that the simulation time of a single step should be relatively small, and that the
pyspectre interface will save the cost of netlist read-in at every step.

\reffig{bash_speed} and \reffig{pyspectre_speed} show histograms of per-step execution times
for the command-line interface and the Pyspectre interface respectively.
\reftab{pyspectre_speed_table} provides the descriptive statistics of the experiment.
In this example, an RL algorithm should expect a 34x reduction in simulation time.


%bash:  DescribeResult(nobs=100, minmax=(558, 3006), mean=811.47, variance=94427.52434343436, skewness=5.136427120560065, kurtosis=30.306425841605446)
%bash:  DescribeResult(nobs=100, minmax=(17.893552780151367, 57.88230895996094), mean=23.773787021636963, variance=20.034626380496583, skewness=4.318918934664504, kurtosis=31.71283545271026)


\begin{table}
	\centering
	\labeltab{pyspectre_speed_table}
	\begin{tabular}{lrrrrr}
		                      & n   & Mean & Min & Max  & St.D.\\
		Spectre CLI interface & 100 & 811  & 558 & 3006 & 307 \\
		Pyspectre interface   & 100 & 24   & 18  & 58   & 4.5 \\
	\end{tabular}
\end{table}



\begin{figure}
	\centering
	\labelfig{bash_speed}
	\includegraphics[width=4.5in]{figures/cliruntimes}
	\caption{A histogram of runtimes of input action steps taken in an LDO Spectre simulation when
		using Cadence's command-line interface.}
\end{figure}

\begin{figure}
	\centering
	\labelfig{pyspectre_speed}
	\includegraphics[width=4.5in]{figures/pyspectreruntimes}
	\caption{A histogram of runtimes of input action steps taken in an LDO Spectre simulation when
		using the Pyspectre interface.}
\end{figure}


\subsection{Conclusion}
One bottleneck to speed is Spectre's file system activity.
To further enhance performance, disk IO should be minimized.
Two ways of approaching this immediately would be to:
either a) attempt the use of command-line options to disable all
logging and simulation results output into the file-system, instead
printing all simulation results to STDOUT and having pyspectre
interpret them there; or b) have pyspectre create the \texttt{\$\{PYSPECTRE\_ROOT\}/simulation}
subdirectory as a ramdisk, so that pyspectre could maintain its contents entirely in system
memory with minimal change elsewhere in the code.

