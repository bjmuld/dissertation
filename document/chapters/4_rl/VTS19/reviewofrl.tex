% !TeX encoding = UTF-8
% !TeX root = ../../muldrey_thesis.tex
% !TeX TS-program = pdflatex
% !BIB TS-program = bibtex
% !TeX spellcheck = en_US

%\begin{figure*}
%	\centering
%	\includegraphics[width=5in]{allStepHists}
%	\caption{Histograms of actions taken by trained DQN agent in PGA environment over 250 timesteps.}
%	\labelfig{stephists}
%\end{figure*}

\section{Review of Machine- and Reinforcement-Learning}
\labelsec{rl}

``Machine Learning'' refers broadly to the study of statistical methods that 
leverage the power of computing hardware to predict attributes of future 
observations by making inferences from past observations, the ``learner's'' 
ability to do so is predicated on 
exposing the learner to a volume of historical data which is representative of
future observations.
%As a discipline, Machine Learning dates back to at least 
%Stanislaw Ulam's and John Von Neumann's 1950s contributions to sampling 
%techniques.
The categories of tools that comprise machine learning are divided into two main classes: those that are ``supervised'' and those that are ``unsupervised.''
A supervised approach refers to the requirement for human intervention in the 
preparation of training data.
%Examples of supervised algorithms include image classification, outlier detection, and voice recognition.
%These kinds of algorithms require training data which has been labeled by hand and is considered the ``ground truth.''
%For example, audio recording data would require associated transcriptions 
%in text form, presumably composed and verified by a human.
%Once a volume of data has been prepared and labeled, the machine can be taught 
%to identify or predict labels from arbitrary input.
%\cite{lippmann_introduction_1987}.
By contrast, ``Unsupervised'' algorithms do not require human intervention.
Classically, unsupervised techniques consist mainly of data clustering 
algorithms.
%K-means, K-nearest neighbor, and self-organizing maps are all examples of machine learning techniques which classify data into clusters based on similarity among data points; there is no need for manually labeling any data. 
%\cite{kohonen_self-organizing_1990}.

%Neural networks as tools for nonlinear approximation emerged as a topic of 
%interest in the late 1980s, however, due 
%to the limitations of computing hardware, were primarily limited to 
%academic exercise.
%%\cite{hornik_multilayer_1989}.
%In 2012, Krizehevsky et. al. captured the attention of the world with their algorithm's breakout performance in classifying images from the ImageNet database \cite{krizhevsky_imagenet_2012}.
%Since then, computer scientists have embraced the neural network and collaborations with hardware engineers presently enable the evaluation and training of very large and very useful neural networks which have given rise to new classes of machine learning algorithms.

\subsection{Reinforcement Learning}

In the early 1990s, work on a third class of machine learning algorithms called ``reinforcement learning'' began to circulate. %\cite{kaelbling_reinforcement_1996}
\cite{suttonReinforcementLearningIntroduction1998}.
In RL, rather than require human intervention on the very granular 
datum-to-datum scale, one provides a means for the machine to evaluate its own 
performance and guide its own learning.
%The machine can then measure its own performance based on observations of its environment and use these signals to guide its learning.
The full potential of RL became evident with the advancement of parallel 
computation tools, and the work of Minh et. al. 
(\cite{mnihHumanlevelControlDeep2015}) 
wherein a RL learner was trained to play the Atari video game console with 
skill surpassing that of humans put RL squarely into the spotlight.
Google's DeepMind team has recently enraptured the public once again with their 
exhibition of a self-taught RL player which has handedly taken the crown from 
the reigning champion, Stockfish \cite{silverMasteringChessShogi2017}.
%Reinforcement learning algorithms presume an environment modeled as an MDP (MDPs explained in \refsec{circuitsasMDPs}).
%which consists of a system with observable states, $s\in\boldmath{S}$, an actor who, at any point, can choose to take one of a set (range) of actions, $a\in\boldmath{A}$, which will result in the system entering a new state, $s'$ with some probability $P(s'|s,a)$ which is time invariant.
The overarching goal of RL is for a machine to learn to be ``good'' at a task 
in a self-directed fashion through feedback about its performance received via 
the reward signal (discussed in \refsec{rlandtesting}).
%Strictly speaking, the only human intervention required is in the provisioning for a reward signal which the algorithm considers ground truth.
%If the task at hand is a game, it might make sense to withhold issuing any reward until the end, in which case there would be zero reward until the game is over and then a ``1.0'' in the event of a win.
%Alternatively, if the strength of a position can be evaluated mid-game, the RL player could receive intermediate rewards for achieving strong positions.


\begin{figure}
	\centering
	\includegraphics[width=4.5in]{figures/stimbuilding}
	\caption{Validation stimulus built as a succession of actions taken, 
		each leading the system into a different state, $S_i$}
	\labelfig{inputevolution}
\end{figure}



\subsection{Basics of (Deep/Double) Q-Learning}

Q-Learning is so named because it centers around the approximation of a 
``quality'' function,

\noindent
\begin{equation}
	q_{i+1}=Q(s_i,a_i)
\end{equation} 

\noindent
which predicts the ``quality'' resulting from taking a certain action, $a_i$, 
given that the system is in state $s_i$.
It requires that every intermediate action, $a_i$, receive a corresponding 
reward signal, $r_i$. In this work, circuit state is defined by a vector of 
node voltages and branch currents.
Individual actions are defined as ramp inputs to the system which take the 
inputs of the circuit under test from an initial value, $a_i$, to $a_{i+1}$ as 
shown in \reffig{inputevolution}.
Starting from the state $s_i$, the RL takes an action (choice of input ramp) 
which will lead the system into state $s_{i+1}$.
We also define $Q^*$, the discounted sum of all $n$
incremental rewards received during an episode:
\noindent
\begin{equation}
Q^*(a_0,\dots,a_n) = \sum_{i=0}^{i=n} \sum_{i}^{n} \gamma^{i} r_i
\end{equation}

\noindent
where $\gamma$ is the factor of time-discounting, which takes a value on the 
range [0,1] (and is usually close to 1).

The goal of Q-learning is to accurately predict $Q$ at each step so that 
optimal actions are selected, resulting in maximization of the expectation of 
$Q^*$.
%One method for Q-learning is to use a neural network to implement an arbitrary function, and then use temporal difference (TD) learning to modify the function over time so that it begins to reflect observations of $Q^*$ with increasing accuracy.
%With a correct mapping of $(s,a)$ into $Q$, at each time step one can choose 
%optimal actions. 
In \cite{watkinsQlearning1992}, the algorithm is shown to 
be convergent to a global optimum. 
In order to correctly model $Q$ however, one must explore the entire 
state-space of the system which can be infeasible even for modestly sized 
digital systems.
So instead, Q-learning algorithms balance exploration and exploitation in effort to deliver acceptable performance in reasonable time. 

``Deep Q-learning'' is simply an implementation of Q-learning which leverages a 
so-called ``deep'' neural net (one which has a number of layers in series 
performing sequential abstractions from observation space to inference space) 
to implement the Q function.
Double-Q learning is another variation in which separate networks are used for 
action selection and Q estimation which can avoid systematic bias introduced by 
guiding learning with the same network that is doing the learning.

%The artificial neural network is constructed so that it takes a number of inputs equivalent to the number of observable states of the system, and through several hidden layers, produces a number of outputs equal to the number of available actions.
%Any time an observation or set of states is presented at the network input, it will output the predicted Q-value for every possible action taken at that time.
%In this way, it's simultaneously predicting outcomes from all possible decisions simultaneously.

%The authors of \cite{mnih_human-level_2015}, for example, made use of several convolutional layers to reduce the high-dimensional image inputs (210x160x3) into lower dimensional abstractions of state, and finally to a discrete action space.
%At the heart of both Q- and deep Q-learning is the Bellman Equation which dictates the update of stored data points used in training the RL learner to approximate the Q function:
%\noindent
%\begin{equation}
%Q(s_i,a_i)' = (1-\alpha)Q(s_i,a_i)  + \alpha (r_i + \gamma \max_{a_{i+1}} 
%Q(s_{i+1},a_{i+1}))
%\end{equation}


In this work, we employed DQN algorithms as described by Minh, et. al. in 
\cite{mnihHumanlevelControlDeep2015} through open source implementations 
provided by OpenAI in their Baselines framework 
\cite{dhariwalprafullaOpenAIBaselines2017} as well as from the 
``stable-baselines'' package \cite{stable-baselines}. 
%Additionally, a simplified algorithm is provided in Algorithm \ref{alg_dqn}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%
%%		Remainder is comment
%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%\begin{algorithm}
%	\label{alg_dqn}
%	\caption{Simplified DQN Algorithm}
%	\begin{algorithmic}[1]
%		\renewcommand{\algorithmicrequire}{\textbf{Input:}}
%		\renewcommand{\algorithmicensure}{\textbf{Output:}}
%		\REQUIRE $Q$, $Q_t$, $env$
%		%\ENSURE  $diagnosis$
%		
%		\WHILE{TRUE}
%			\FOR{i = [0, $updateInterval$]}
%				\FOR{j = [0, $trainInterval$]}
%					\STATE $obs$ = reset($env$)
%					\FOR{k = [0, $stepsPerEpisode$]}
%						\STATE $act$ = getBestOrRand($Q_t$, $obs$)
%						\STATE $rew$, $obs'$ = takeAction($act$)
%						\STATE storeExperience($obs$, $act$, $rew$, $obs'$)
%						\STATE $obs$ = $obs'$
%					\ENDFOR
%				\ENDFOR
%				\STATE $mems$ = sampleExperiences()
%				\STATE $error$ = computeBellmanError($mems$)
%				\STATE $q$, $loss$ = optimizeNet($Q$, $mems$, $error$)
%				\IF{$loss < thresh$} \STATE exit() \ENDIF
%			\ENDFOR
%			\STATE $Q_t$ = $Q$
%		\ENDWHILE
%	\end{algorithmic}
%	
%	%\noindent
%	{\textbf{Definitions:}}
%	
%	\begin{itemize}[
%		\setlength{\IEEElabelindent}{-6pt}%
%		\setlength{\itemindent}{0pt}% identation for each new \item
%		\setlength{\listparindent}{0pt}%
%		]
%		\item[] $Q$: Q-function network, randomly initialized. Used to calculate Bellman error.
%		\item[] $Q_t$: target Q-function network, randomly initialized. Used to select action.
%		\item[] $env$: circuit-pair, simulated in unison
%		\item[] $updateInterval$: frequency of update of target net.
%		\item[] $trainInterval$: frequency of optimizing model net.
%		\item[] $obs$: set of paired circuit state observations.
%		\item[] $stepsPerEpisode$: predetermined number of actions that constitute a stimulus.
%		\item[] $act$: selected action to apply to the circuit input.
%		\item[] $getBestOrRand()$: returns action selected from target Q-function network and selection policy.
%		\item[] $takeAction()$: applies the given action to the circuit and simulates a time step, returning next state.
%		\item[] $storeExperience()$: saves the experience into memory
%		\item[] $sampleExperience()$: samples experiences from memory
%		\item[] $computeBellmanError()$: returns Bellman update vector from memories
%		\item[] $loss$: network performance metric; includes regularization
%		\item[] $optimizeNet()$: performs optimization of network given memories and error vectors.
%		\item[] $thresh$: termination criterion.
%
%	\end{itemize}
%\end{algorithm}


%\subsubsection{(Deep) Policy Gradient Methods}
%Policy gradient RL methods utilize a policy function, $\Pi(a|s)$, which 
%will chose an action $a_i$ with probability $\pi_i$, thus mapping  
%states directly into the action space
%(by contrast, the Q function maps $(s_i ,a_i)$ into an expected quality or 
%value).
%The policies themselves can be rendered deterministic by restricting the 
%values 
%of $\pi$.
%If the policy mapping function is parameterized by a vector, $\theta$, then 
%gradient ascent can 
%be performed and, provided a measure of relative 
%desirability 
%can be calculated, will result in convergence on a locally optimal policy
%\cite{sutton_policy_2000}.
%
%However, evaluating the gradient of the relative value of a policy is 
%difficult 
%in large state spaces.
%Monte Carlo sampling of the rewards under a certain policy can yield estimates 
%of the gradient.
%The authors of \cite{jaakkola_reinforcement_1995}
%showed that provided an approximator
%meets certain criteria, one can be used to provide approximate gradients which 
%will still ensure convergence.
%MC sampling along with a neural approximator can be used in formulating an
%``Actor-Critic'' methodology, where one neural approximator implements a 
%policy 
%and another provides gradient estimates enabling incremental policy update.


