% !TeX encoding = UTF-8
% !TeX root = ../../muldrey_thesis.tex
% !TeX TS-program = pdflatex
% !BIB TS-program = bibtex
% !TeX spellcheck = en_US



In this chapter, we reformulate the systems under validation as a Markov 
decision process and examine the use of reinforcement-learning to provide a 
globally convergent solution, a means of ``storing'' the valuable 
information created during stimulus generation, and low-cost iterated 
generation. 
The integration of the proposed design validation methodology with 
deep-Q learning software and the suite of Cadence simulation tools is 
presented, validation results for selected design bugs in representative 
designs are analyzed, and the quality and efficiency of the 
proposed design validation methodology is discussed.

%High operating speeds and use of aggressive fabrication technologies 
%necessitate validation of mixed-signal electronic systems at every stage of 
%top-down design: behavioral to netlist to physical design to silicon. At 
%each step, design validation establishes the equivalence of lower level 
%design descriptions against their higher level specifications. 
%Prior research has leveraged state reachability analysis, nonconvex 
%optimization, or performance specifications in order to generate tests. 
%%Most methods employ down-selection of test stimuli from a known given set 
%%of possible stimuli.
%%	In contrast, we reformulate the systems under validation as a Markov 
%%	decision process and discuss a reward metric for reinforcement learning 
%%	based on the \emph{difference between the response of the high level 
%%model
%%	and its low level implementation}.






\section{Introduction}
% no \IEEEPARstart
\labelsec{intro}

%Modern integrated-circuit and system-on-chip design largely proceeds in a ``top-down'' fashion, meaning the system is first described at its highest level of abstraction, and design details and performance
%specifics become fleshed out in an increasingly specific manner (\reffig{topdowndesign}).
%%Specifications of constituent modules are specified with increasing levels of detail as the design progresses through transistor-level netlist and eventually layout (\reffig{topdowndesign}).
%%With design decisions being made often and throughout the process, it's necessary for designers to be sure to update their understanding of the combined behavior and performance of all subsystems and components as they're continually being updated.
%One fundamental challenge is in mixed-mode simulation; the solvers and numerical simulation techniques which model the higher-level blocks aren't necessarily the same ones used to evaluate the low-level descriptions.
%Another complementary challenge is what combination of subsystem implementations and high-level blocks should be assembled for testing and what input stimuli should be applied.
%Both of these challenges emerge from the reality that conducting chip-scale spice-level simulations is too computationally costly to be feasible into the foreseeable future.
%In this work, we bring reinforcement learning to bear on these problems for the first time.

In hierarchical system design (SoCs, SoPs), models for mixed-signal components 
(e.g. regulators, data converters, I/O, RF ) are described at the behavioral 
level (Simulink or MATLAB) with the complete system described as an 
interconnection of these models.
The design strategy is typically top-down with bottom-up verification 
of synthesized netlists and physical layout.
The design is incrementally advanced and verified with the intent of ensuring 
design correctness through each iteration of the design process from high level 
design specification to physical layout to silicon. 
This prevents expensive correction of design bugs that 
percolate from early stages of the design process to later steps including 
fabrication of silicon.
To facilitate rapid turnaround design, it is necessary to verify behavioral 
equivalence between a higher level specification of the design (e.g. AHDL) and 
its lower level implementation (e.g. netlist) as early as possible.

%Contemporary system designers find themselves spending an increasing amount of 
%resources on design debugging, functional verification, and 
%post-silicon validation \cite{keshava_post-silicon_2010} and 
%\cite{_prologue_2016}.
While the analog specifications of modules at two different levels of the design
can be checked for equivalence, such a check does not guarantee behavioral 
equivalence across the entire space of input stimuli because the set of 
specifications used to check for equivalence may itself be \textit{incomplete} 
\cite{deorioMachineLearningbasedAnomaly2013, 
guChallengesPostsiliconValidation2012}.
For similar reasons, formal methods for design equivalence checking may fail because designer inserted assertions may not cover all input-output behaviors and are generally myopic and constrained to specific input conditions. 

\section{Prior Work}
The use of rapidly exploring random trees (RRT) for analog circuit test 
generation was presented in 
\cite{ahmadyan_goal-oriented_2012,ahmadyan2013runtime}.
The idea was to quickly explore reachable points in the state space of the 
analog circuit for verification purposes.
In \cite{steinhorst_improving_2010}, an efficient discretized state space guided
test stimulus generation approach is proposed with the
goal of equivalence property checking.
The methodology combines formal methods
with circuit simulation techniques.
In a similar vein, the equivalence between a behavioral model and its 
transistor level design over a highly likely input stimulus space is discussed 
in \cite{singh_behavioral_2010}.
The discrepancy between the two design descriptions over the
space of possible input stimuli is maximized to detect design errors.
The work of 
\cite{hu2018hfmv,yin2012verifying,lin_verification_2013} was 
among the first to combine formal verification methods with simulation driven 
techniques to explore the limits to which analog design specifications can be 
stressed, but assumes tests that are derived from manually crafted 
specifications.
\noindent
%While the techniques of \cite{ahmadyan2012goal,ahmadyan2013runtime} are 
%interesting,
%they do not explicitly focus on directing test generation towards 
%-the rare input stimuli that force differences between expected and observed 
%-behaviors in carefully tailored designs (the observation of such events is 
%the 
%-only way to disprove validity).
%-In contrast, the approach of \cite{hu2018hfmv} addresses the rare design 
%-defects that can cause anomalous behavior but assumes tests that are derived 
%-from manually crafted specifications, which leaves holes in the verification 
%-process and makes it vulnerable to human error.
\noindent
Central challenges going forward include eliminating up-front assumptions of 
input distribution (i.e. ``kinds of input'') and assumptions of completeness of 
any set of provided specifications.


\section {Motivation}

The purpose of this work is to present a new reinforcement learning (RL) driven 
test generation
and anomaly-model generation algorithm that does not require any a-priori 
knowledge about:
stimuli to be used for design verification/validation, the device
specifications, or properties to be verified. 
%The adversarial learning algorithm proceeds autonomously, requiring few 
%parameters.
Additionally, the RL techniques used have been shown to be convergent upon the 
global optimum under certain conditions and internally contain a useful 
distillation of all previous information (they have memory) and so can be 
reused and retrained many times and in many 
contexts at higher efficiency than optimization 
alone \cite{watkinsQlearning1992}.


\begin{figure*}
	\centering
	\includegraphics[width=5.25in]{figures/topdowndesign}
	\caption{Illustration of a top-down design process}
	\labelfig{topdowndesign}
\end{figure*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  Amplifier example:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%For example, one designing an amplifier for reproducing music 
%through a loudspeaker would first consider the most basic 
%characteristics desired: power output, gain, and distortion.
%Competing architectures and their constituent subsystems would be reviewed for 
%their 
%ability to achieve the desired performance.
%Individual subsystems, e.g. pre-amplifier and power-amplifer stages, are then 
%prescribed their own specifications corresponding to the chosen architecture.
%The choice of both architecture and sub-component specification bear 
%or system-level performance.
%Eventually, netlists are created to implement all the various subcircuits, 
%layouts are generated, and the system is fabricated.
%At a given point in the process there are any number of subsystems and 
%components in varying states of completion which can be simulated individually 
%on test benches or in concert as a composed design.
%
% can be repeatedly validated 
%against high level specifications before layout extraction, final validation, 
%and fabrication.
%
%Already, the designer can identify the major subsystems necessary: 
%a power supply subsystem, a user-interface subsystem, an input gain stage, 
%et cetera.
%He will immediately assemble crude models of each subsystem into a crude 
%simulation
%model of his amplifier.
%Even these simple models can alert him to problems in his design. 
%Perhaps 15dB of gain in the input stage and 10dB in the output stage would 
%not meet his overall gain requirements.
%After identifying suitable subsystem specifications, the designer would 
%descend the 
%hierarchy of details, identifying and modeling the major components of each 
%subsystem, continually verifying the performance of the system.

Traditionally, design requirements are defined by putting bounds on the
values of many individual performance metrics and testing them explicitly.
This method can clearly define what is valid and what is not;
however, it is very difficult to completely constrain acceptable behavior of a 
system through performance specification, and complete sets of performance 
specifications are rarely available during design implementation.
Though occasionally system-level performance may be calculable as an analytical 
function of subsystem parameters, this is not generally the case,
and is confounded by components which exhibit memory effects, 
nonlinear behavior, or high-order physical phenomena.
Contemporary system designers find themselves spending increasing 
quantities of resources on design debugging, functional verification, and 
post-silicon validation \cite{keshava_post-silicon_2010} and 
\cite{_prologue_2016}.
The authors of both \cite{deorioMachineLearningbasedAnomaly2013} and 
\cite{guChallengesPostsiliconValidation2012} 
refer to corner cases and anomalies for which validation results are 
inconsistent with performance metrics:
\begin{enumerate}
    \item  Designs may meet existing performance specifications under certain 
    tests, but fail in the anomalous case
    
    \item Designs may not meet a particular performance specification, but 
    only because the desired specification is unattainable in practice.
    Should such cases exist, it is the goal of design validation to 
    identify them.
    Equivalently, if we can reject the claim that the system we've produced is 
    equivalent to our intent, we wish to do so as quickly as possible.
\end{enumerate}

Explicitly, these tasks require the evaluation and comparison of the 
dynamical behaviors of two descriptions of the same system.
If one can implement an ideal opamp using transistors, then any system 
containing opamps will be agnostic between the ideal model and the 
transistor implementation.
Unfortunately, no transistor opamps are ideal.
And so, as the descriptions of compositional blocks of systems become 
increasingly detailed, they become decreasingly ideal, and their behaviors 
stray further from their more 
abstract and idealized models.

This paradigm extends thorough fabrication, which can be thought of as 
the ``most detailed'' description of a system that can be achieved.
Both validation and verification seek to first quantify the degree of 
behavioral departure from the architect's original intent and then
classify it as either ``valid'' or ``invalid,'' verification having been 
``passed'' or ``failed.''

Design validation addresses the testing, evaluation, and comparison of the  
behaviors of the various subsystems and components expressed in different 
ways across the design hierarchy.
Pre-silicon validation (conventionally ``verification'') involves evaluation 
and comparison of simulation models of systems, subsystems, and components at 
one description level against simulation models at other description levels.
Post-silicon validation involves evaluation and comparison of 
the dynamical behaviors of simulation models of systems, subsystems, and 
components against the dynamical behaviors exhibited by physical systems.
Both pre-silicon and post-silicon validation (pre-silicon being synonymous with 
design verification) seek to quantify the degree of behavioral departure from 
the designers' intent.
It is the goal of AMS system validation to either support or reject the claim 
that a detailed system design (either physically or in simulation) meets the 
requirements of the system specification more broadly.

%\textbf{
%The purpose of this work is to evaluate the potential of reinforcement learning in creating an artificial intelligence able to produce high-performance stimuli for use within a larger validation framework.
%}
%Because demonstrating a validation ``fail'' does not require a global optimum 
%nor does it require complete convergence, a high improvement rate increases 
%the 
%likelihood of discovering a validation fail, which decreases the expected 
%time-to-fail, 
%leading to shorter design revision cycles.



In \refsec{rl}, we briefly review the subject of RL and its relationship to 
other machine learning (ML) techniques.
In \refsec{circuitsasMDPs}, we present our formulation of the analog and 
mixed-signal (AMS) system validation problem as a Markov decision process 
(MDP).
In \refsec{rlandtesting}, we illustrate the direct applicability of 
contemporary RL techniques to validation and explore the possible benefits over 
existing techniques.
In \refsec{results}, we detail our implementation of deep
Q-learning and discuss our experimental setups.
We present experimental results and provide a summary 
analysis before concluding the paper in \refsec{conclusion}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subinputfrom{/}{rlandtesting.tex}
%\subinputfrom{/}{overview.tex}
\subinputfrom{/}{reviewofrl.tex}
\subinputfrom{/}{circuitsasMDPs.tex}
\subinputfrom{/}{methodology.tex}
\subinputfrom{/}{results.tex}
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Conclusion}
\labelsec{conclusion}

We have presented the details of our state-of-the-art 
reinforcement learning algorithm which serves as the stimulus-generation 
subsystem 
in a design validation framework.
We have designed and conducted several experiments which leverage Q-learning to generate a stimulus which is capable of revealing a maximal amount of information about the differences between two systems.
Our work suggests that reinforcement learning provides a very powerful 
complement to tools like differential evolution and Monte Carlo stimulus 
generation.

While DQN is able to ultimately outperform the other methods, it comes at the 
cost of computational time (about 10x more).
It however outperforms the optimization solver from a warm-start by a factor of 
roughly 1800x, suggesting that if appreciable restarts are foreseen, the 
benefits of
RL may outweigh its up-front costs.

%The fundamental characteristic of RL and \textit{deep} RL generally, is their dependence on large volumes of data.
%There are two ways to generate this data and they are: 1) power up a physical circuit and apply stimuli, or 2) build a simulation model.

%The evidence suggests that the validation tool should be matched to the 
%validation task.

Some validation tasks can be accomplished with traditional specification tests 
(i.e. two-tone test) while others might require slightly more entropic stimuli 
like white noise in order to excite behaviors of interest.
Circuit complexity, situation, or mission-criticality may mandate running the 
additional number of simulations required to train a high-performance agent to 
reveal the global solution, something that stochastic nonconvex optimizers 
cannot provide.
We will continue this work, looking at broader classes of circuit and varying 
degrees of bug magnitude and nature.
Additionally, we will explore the limits of RL's warm-start advantage.

%There are classes of circuit which require continuous input, and for these, DQN is ill-suited.
%By its nature, DQN selects actions from a discrete set of available actions -- attempting to discretize circuit inputs to even modest levels will result in an explosion in the number of parameters in the network.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% use section* for acknowledgement
\section*{Acknowledgment}
The authors would like to thank the 
Semiconductor Research Corporation for supporting this
research under GRC Task 2712.005. This project is funded
under the Texas Analog Center of Excellence (TxACE) at UT Dallas.


% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://www.ctan.org/tex-archive/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/



	
