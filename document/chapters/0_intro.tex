% !TeX encoding = UTF-8
% !TeX root = ../muldrey_thesis.tex
% !TeX TS-program = pdflatex
% !BIB TS-program = bibtex
% !TeX spellcheck = en_US


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                           %
%                                           %
%        INTRODUCTORY  section/chap         %
%                                           %
%                                           %
\chapter*{Introduction}
\labelchap{intro}                           %
\addcontentsline{toc}{chapter}{Introduction}  %add References section to toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Advancements in semiconductor manufacturing have enabled the
production of transistors so small that they cease to behave 
deterministically;
they have also enabled the integration of increasingly diverse kinds 
of systems onto smaller and smaller pieces of silicon.
These technological developments have been the primary mechanism through
which greater electronic system performance and lower system cost have been 
achieved.
Simultaneously, design tools have progressed in parallel
with manufacturing techniques, focused on managing increasing transistor counts.
Tools were developed to allow relatively small teams of perhaps
hundreds of engineers design and manipulate the interactions of billions of
transistor devices.
Testing methodologies were developed so that the designers
could quantify their confidence in their output at various stages.
Historically, testing techniques have centered around direct measurement
of performance metrics.

Until recently, the trajectories of process, design, and testing tools
have consistently paid dividends. 
Now, component-level designs are failing to integrate, system-level performance 
suffers from unforeseen component interactions, and fabricated chips 
exhibit behaviors that no model has predicted.
Systems spend more time in debugging and diagnosis phases, and preproduction 
engineering costs are increasing.
After reaping the low-hanging fruit of planar transistor scaling, designers of 
both electronic systems and design tools face new sets of 
challenges \cite{keshava_post-silicon_2010}.

There are three complementary factors contributing to the frustration of
existing approaches.
For one, shrinking device sizes and shrinking power budgets leave components
more sensitive to interactions with neighboring components.
Interactions between components are difficult to model because they are often
a function of downstream decisions like chip layout,
and because they require simultaneous simulation of all interacting components.
For another, extreme performance expectations require operation within increasingly
narrow performance tolerances (i.e. higher transmission rates generally require
lower noise).
This means that all subsystems must be increasingly robust against smaller and
smaller mechanisms which might disrupt their performance -- disruptions which
are difficult to predict and model.
Third is the simulation complexity problem.
In order to simulate larger numbers of interacting components in reasonable time,
the computational complexity of individual component models must be reduced.
High-order behavioral phenomena should be abstracted away to expedite evaluation;
however, it is some of these ``high-order effects'' which are leading to
downstream integration problems;
they are increasingly relevant to overall system performance.

Because compact analytical expressions are not readily available for low-level
models, practicable solutions must operate with a basis in the discrete
empirical observational data resulting from simulation or otherwise
acquired through experimentation.
It should be emphasized though, that the observational data
are reflective of both the properties of the system under analysis \textit{and
properties of the test stimulus}, and so a mechanism for
distinguishing between inferred system properties and properties of particular
stimuli is required.
A complete suite of tools should provide guidance for simulation setup or
experimental design and test stimulus selection as sell as
metrics for measuring the quality or utility of the resulting data.
Such a measurement implicitly provides a measure of quality for the test
itself (setup and stimulus).


The fundamental contributions of this work are:
\begin{itemize}
    \item The synthesis of test stimulus generation and incremental model 
    building applied to the system validation problem on the dynamical level.
    
    \item A holistic approach toward fault localization which does not require 
    invertiblity of system components.
    
    \item The demonstration of the aforementioned methodologies in both pre- 
    and post-silicon validation contexts.
    
\end{itemize}

The organization of this thesis in intended to follow the chronological arc of 
my research.
It begins with a very acute focus on test stimulus design, in the vein of the 
``alternate testing'' of my forebears, then explores reinforcement learning as 
a mechanism for capturing and utilizing the ``data exhaust'' of iterative 
optimization, and concludes with a more holistic view of test data and 
\textit{data quality} in a chapter exploring the application of a 
design-of-experiments approach to test design.

\refchap{background} presents a summary of the state-of-the-art in 
hierarchical circuit design and modeling, design 
validation, and diagnostic techniques as they apply to \gls{ams} systems.
It discusses in depth the factors that threaten the continued success
of status-quo approaches to AMS design and validation and critiques several
proposed trajectories of work.
\refchap{optimization} discusses several optimization-based resolutions to some of the
contemporary challenges.
\refchap{rl} presents an investigation of ways contemporary artificial intelligence
tools might be of use.
\refchap{doe} presents an alternative formulation of some of the challenges facing AMS
validation as a ``design of experiments'' problem, and applies some statistical tools 
from that domain.
\refchap{diagnosis} outlines the extension of validation techniques into fault localization and 
debugging applications.
\refchap{software} details several substantial software products which have been developed and opensourced
for use by both AMS practitioners and the greater scientific community.
%\refchap{future} discusses what challenges lay ahead and which paths are most 
%likely to bear fruit.

