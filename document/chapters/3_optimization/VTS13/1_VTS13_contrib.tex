% !TeX root = ../../../muldrey_thesis.tex


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%   Chapter: Optimization Approaches
\section{Hardware-based Guided Stochastic Test Stimulus Generation}
\labelsec{ravage}
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%\subsection{Summary}
In this section, I
% was presented at the 2013 VLSI Test Symposium;
introduce the RAVAGE algorithm (from ``random,'' ``validation,'' and 
``generation''),
which uses a fabricated AMS hardware system to generate its own test stimuli to 
be used for post-silicon 
validation of mixed-signal
systems \cite{muldrey_ravage:_2013}.
The approach of RAVAGE is new in that no assumptions are made beforehand about 
the nature of the anomalies which the test seeks out within the 
\gls{dut}; but rather, the stimulus is generated using the \gls{dut} 
itself.
The objective of RAVAGE is to maximize the magnitude of any behavioral 
differences between the \gls{dut} (hardware) and its behavioral model (software)
observable in their responses to the same stimulus.
Stochastic search methods are used since the exact nature of any behavioral 
anomaly in the \gls{dut} cannot be known a priori.
Once a difference is observed, model parameters are tuned using nonlinear 
optimization algorithms in attempt to resovle the difference in responses and 
the process (test generation $\rightarrow$ tuning) is repeated.
If a residual error remains at the end of this process that is larger than a 
predetermined threshold, then it is concluded that the \gls{dut} contains 
unknown
and possibly malicious behaviors that need further investigation.
Experimental results on an RF system (hardware) are presented
to prove feasibility of the proposed technique.

\subsection{Algorithm}
The RAVAGE stimulus generation engine provides a means of performing 
\textit{on-the-fly} test stimulus generation which is a function of not only 
the model but also the \gls{dut} itself.
RAVAGE stochastically searches the operating space of a single \gls{dut} with 
the 
goal of stressing it to the point that equivalence to its model can either be 
rejected, or be asserted with some certainty.
Whether or not the process corner of the \gls{dut} is known, whether or not the 
\gls{dut} 
is suspected of errant behaviors, RAVAGE may be used to validate (or 
invalidate) a \gls{dut} against its model.

\begin{figure}
	\centering
	\includegraphics[width=2.8in]{figures/flowchart.pdf}
	\caption{RAVAGE System Overview}
	\label{fig_flowchart}
\end{figure}

The RAVAGE algorithm begins with a population of arbitrary band-limited 
stimuli 
and employs a genetic algorithm to evolve the population (within amplitude and
frequency constraints) in such a way that it best excites behaviors in the 
\gls{dut} 
that the 
\gls{dut}'s model does not exhibit. Whether due to structural or topological 
inadequacy of the model (equivalent to unexpected circuitry in the \gls{dut}), 
lack 
of complexity in the model, or process variation in the \gls{dut} 0(equivalent 
to 
incorrect model parameters), RAVAGE will favor those stimuli which best expose 
differences, regardless of their origin. If stimulus performance improvement 
becomes static, RAVAGE attempts to tune the model to capture the observed 
discrepancy.
The stimuli used for tuning 
are then put aside, the entire population is reinitialized with random seeds, 
and the process repeats. If RAVAGE can tune the model sufficiently, such that 
error power never exceeds a threshold, then the model can be treated 
as complete, both behaviorally and parametrically. RAVAGE can 
accept a model of any complexity simulated in Matlab or Spectre and can 
interface with Spectre to tune spice models.

\subsubsection{Stimulus Implementation}
A population, $G$, of $i$ individual stimuli is created. Each individual 
stimulus, $s_i$, is initialized to randomly generated white Gaussian noise. 
The 
length of $s_i$ is determined by the desired bandwidth of excitation, and they 
are considered to be sampled at the Nyquist rate.  These stimuli will be 
undergoing mutation in the frequency domain, and the applied stimuli, $S_i$ 
are 
derived from this reference population before each use. To arrive at $S_i$, 
each $s_i$ undergoes low-pass interpolation, additional low-pass filtering, 
application of an envelope, DC removal, and normalization to the 
desired amplitude. $S_i$ and $s_i$ are kept at differing sample rates so that 
stimuli can be applied using high sampling rate arbitrary waveform generators 
(AWGs) while allowing the genetic algorithm to operate only over the bandwidth 
of interest. The interpolation ratio is decided depending on the maximum 
desired frequency resolution of the stimulus and the sampling rate of the AWG. 
Interpolation is performed by injecting $L$ zero-samples between each original 
sample and subsequently performing low-pass FIR filtering. An amplitude mask 
is 
then applied to the stimulus, to diminish out-of-band high frequencies 
resulting from instantaneous envelope transitions. A hyperbolic tangent 
sigmoid 
vector is multiplied with the first and last $w$ samples:
\begin{subnumcases}{s'(t)=}
	\dfrac{1}{1+e^{-k\cdot s(t)+5}} , & for $1 \leq t \leq w$\\
	\dfrac{1}{1+e^{k\cdot s(t)-5}} , & for $T -w \leq t \leq T$\\
	s(t) , & otherwise
\end{subnumcases}\\
\noindent where $k$ controls the rise-time of the envelope.\\

Additional low-pass filtering is performed using a Chebyshev(I) low-pass 
IIR filter to remove any aliasing remnants from the interpolation operation 
and 
envelope application. Finally, DC is removed, and the signal is normalized, 
guaranteeing to reach but not exceed a user-specified amplitude. Both the 
low-pass filtering and gain of the stimuli are designed based on the expected 
system specifications. Each member of the population $S_i$ is then zero-padded 
and concatenated for application to the \gls{dut}. The concatenated stimuli 
are sent 
through the AWG and the \gls{dut}, and the response is captured by a 
high-speed 
digitizer. At the same time, $S$ is used in transient simulation on the model 
of the \gls{dut}.

\subsubsection{Fitness Function, Biasing, and Error Metrics}
The bias provided by an effective fitness function is critical to the the 
evolution of the stimuli. RAVAGE's fitness function for evolution is as 
follows: The DFT of the \gls{dut}'s response, the model's response, and the 
applied 
stimulus, $S_i$, is calculated. The power-specral density (PSD) is then 
computed for each, and the 
model's PSD, $P_m$ is subtracted from the \gls{dut}'s, $P_d$. This error power 
vector, $P_e$ represents output power differences between 
the \gls{dut} and its model. For the remainder of fitness and error metric 
calculation, only a predetermined bandwidth of interest (BOI) is considered. 
The $L_2$ norm 
of this vector over the BOI is kept as a measure of each stimulus' 
effectiveness. The stimuli that yield the largest error are preserved from 
generation to generation. This error metric, however, does not function as the 
fitness function for genetic evolution. Instead, the population is groomed not 
only to maximize error power, but is penalized for yielding a spectrally 
sparse 
error power and is rewarded as 
stimulus power, $P_s$ decreases. So, in addition to the scalar error power 
metric, a fitness value, $F(S_i)$, is calculated for each stimulus:

\begin{equation}
F(S_i)= \begin{array}{cc}
\underline{\|P_{e_i}\|_{_2} D(P_{e_i})}\\
{ P_{e_i}}
\end{array}
\end{equation}

\noindent Where $D(P)$ gives a scalar representation of the spectral 
``richness'' of a PSD vector on the range $[0,1]$ ($N$ here is the number of 
frequency buckets over the bandwidth of interest):

\begin{equation}
D(P_{e_i})=N \left\|  \begin{array}{c}\underline{P_{e_i}}\\
max(P_{e_i})
\end{array}
\right\|_{_2}
\end{equation}
\noindent The $\|P_{e_i}\|_{_2}$ of each stimulus is termed \textit{total 
error 
power}.
The reference stimuli are then ranked. The top $\eta$ performers in scalar 
error power are ranked first, followed by the rest in order of descending 
fitness. 
Additionally, the greatest total error power of each generation is monitored, 
and if it is seen to stagnate for a period, RAVAGE is determined to have 
reached \textit{stasis} and will either begin model tuning or terminate. 
Otherwise, genetic mutation is performed over the population, and the process 
repeats.

\subsubsection{Population Genetics}
The absolute performance of a stimulus is not measured by the fitness function 
that guides its evolution, but rather it's measured by the total error power 
the stimulus yields. The best $\eta$ stimuli (according to the previously 
explained rankings) are preserved and are subsequently known as \textit{elite} 
stimuli.  The elite, along with the next $\epsilon$ are then considered 
\textit{survivors}. The remainder are destroyed to make room for new stimuli 
(total population size $i$ is constant). New stimuli are created in three ways 
(Fig.\ref{heredityFig}):

\begin{enumerate}
	\item All of the survivors mutate, including the elite (though the elite 
	will propagate unmutated as well), to yield \textit{mutants}.
	\item Pairs of survivors are arbitrarily selected to procreate. Their DFTs 
	are split at a random frequency and are spliced together to create the 
	\textit{children}'s DFT which subsequently undergoes an inverse DFT.
	\item \textit{Alien offspring} are introduced which are the product of 
	procreation of one survivor with an \textit{alien} stimulus (a new white 
	gausian stimulus) in a similar manner to 2).
\end{enumerate} 

\begin{figure}[!t]
	\centering
	\includegraphics[width=4.33in]{figures/Diagram1a_cropped.pdf}
	\caption{Population Heredity}
	\label{heredityFig}
\end{figure}

\noindent
Mutation is performed over the survivors by separately and arbitrarily 
disturbing the magnitude and phase of each stimulus' DFT before taking the 
inverse-DFT. Each amplitude is scaled randomly based upon a Gaussian 
distribution centered around unity and each phase is mutated by adding random 
phase based upon a Gaussian distribution centered at zero. In each case, the 
distributions' $\sigma$ is globally controlled and termed \textit{mutation 
factor}. This new generation of reference stimuli are processed, as previously 
described, before application to the \gls{dut} and model.


\subsubsection{Model Tuning}
After some number of generations, successive improvement in total error power 
between generations will reduce and approach zero. The genetic algorithm has 
reached a local maximum, and will likely not begin to improve again.  This is 
a 
good opportunity to take advantage of the current population of stimuli to 
tune 
the model parameters to reduce the total error power. The model (which exposes 
some number of ``knobs'' to the solver, 
thereby enabling use of models of any detail), a subset of high performing 
stimuli, and their corresponding \gls{dut} responses are passed to a 
non-linear 
least 
squares solver. The model parameters which the solver finds to yield minimal 
total error power are selected as candidate parameters. The candidate 
parameters' performance is compared to performance of the previous parameters 
using the remainder of the current population, and if improved, the new 
parameters are kept.

\subsection{Completed Experiments}
\subsubsection{Test Equipment}
RAVAGE was implemented in Matlab 2011b on a conventional Dell quad-core 
Xenon workstation. National Instruments LabView is run concurrently with 
Matlab 
and is responsible for communication with the test instruments. Matlab scripts 
control states of variables within a Matlab-VI in LabView, thereby controlling 
the test instrumentation. A National Instruments PXI-1073e chassis houses the 
AWG and digitizers, respectively a PXI-5412 and PXI-5105. National Instruments 
the PXI-5412 AWGs' square-wave output can be trusted to 5 MHz, and the 
PXI-5105 
is operated with its 24 MHz anti-aliasing filer in place. The sample clock 
PLLs 
in the two cards are both referenced to the 10 MHz backplane synchronization 
signal, and a sample time accuracy of much less than one sample is achieved at 
60 MHz sampling rates. More important than triggering delays, however, is the 
relative agreement of sample clocks. Because processing is occurring entirely 
in the frequency domain, any frequency translation resulting from disagreeing 
sample clocks is hazardous.


\paragraph{DUT=wire ,  MODEL=wire :}\label{exp:ravage1}

In this experiment, cables directly connect the output of an \gls{awg} to the 
input of a \gls{digitizer} and the system is modeled by an 
ideal wire.
A Photo of \textit{Experiment 1}'s 
hardware 
setup is provided in Fig.\ref{wirewirephoto}.
The system is modeled by
an ideal wire.
Using a maximum stimulus bandwidth of 5 MHz, and an error BOI of 18 MHz, 
RAVAGE was allowed to run for 853 generations.
This experiment was run primarily to determine the effective baseline total 
error power.
Fig.\ref{wire_wire_results} shows the fitness and total 
error power over 400 generations.
The stimulus that achieved the maximum error is shown in Fig.\ref{wirewirestim}.

\begin{figure}
	\centering
	\includegraphics[width=4in]{figures/wirewirePhoto.pdf}
	\caption{Experiment 1 \gls{dut}}
	\label{wirewirephoto}
\end{figure}

\begin{figure}
	\centering
%	\begin{minipage}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=4.5in]{figures/wireWireResults.pdf}
		\caption{Experiment \ref{exp:ravage1}(a) Results}
		\label{wire_wire_results}
\end{figure}
\begin{figure}
%	\end{minipage}
%	\hfill
%	\begin{minipage}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=4.5in]{figures/wirewirestim.pdf}
		\caption{Best Stimulus from Experiment \ref{exp:ravage1}(a)}
		\label{wirewirestim}
%	\end{minipage}
\end{figure}


%\subsubsection{Experiment 2}
\paragraph{DUT=up/down mixers ,  MODEL=wire :}\label{exp:ravage2}
In this experiment, a pair of Maxim MAX2039 high-linearity up- and 
down-conversion mixers are inserted between the \gls{awg} and \gls{digitizer} 
but the model 
remains an ideal wire.
Care was taken to 
ensure 
phase coherence at each mixer's LO input.
Fig.\ref{converterphoto} provides a photograph of the MAX2039s while under test.
The model in this experiment was left undisturbed from \textit{Experiment 1}.
Using a maximum stimulus bandwidth of 5 MHz, and an error BOI of 18 MHz, 
RAVAGE ran for 623 generations.
Fig.\ref{wireconverterresults} shows the fitness and total error power of the 
population through 60 generations of \textit{Experiment 2},
and Fig.\ref{exp2stim} shows a time-domain plot of the 
most effective stimulus.
\textit{Experiments 1} and \textit{2} 
collectively give RAVAGE an effective pass/fail discrimination SNR of 32.5 dB 
based only on the nonidealities exhibited by Maxim's high linearity 
converters.

\begin{figure}
	\centering
	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[width=2in]{figures/convertersPhoto.pdf}
		\caption{Experiments \ref{exp:ravage2}(b) and \ref{exp:ravage2}(c) 
		\gls{dut}}
		\label{converterphoto}
	\end{minipage}
	\hfill
	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[width=2.49in]{figures/wireConverter.pdf}
		\caption{Experiment \ref{exp:ravage2}(b) Results}
		\label{wireconverterresults}
	\end{minipage}
\end{figure}

\begin{figure}
	\centering
	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[width=2.49in]{figures/wireconverterstim.pdf}
		\caption{Best Stimulus from Experiment 2}
		\label{wireconverterstim}
	\end{minipage}
	\hfill
	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[width=2.49in]{figures/exp2stimcrop.pdf}
		\caption{Best Stimulus from Experiment 2}
		\label{exp2stim}
	\end{minipage}
\end{figure}

%\subsubsection{Experiment 3}
\paragraph{DUT=up/down mixers ,  MODEL=up/down mixers :}
This experiment was designed to test the effectiveness of 
iterative model solving throughout the RAVAGE process and to evaluate the 
ability of the non-linear solver to arrive at parameter values using a 
RAVAGE-produced stimulus as compared to \cite{banerjee_optimized_2011}.
As in \textit{Experiment 2}, the \gls{dut} remains sequential up-conversion and 
down-conversion by a pair of MAX2039 converters with phase coherent LO inputs.
In this experiment, however, a $5^{th}$ order polynomial model is used to model 
amplitude nonlinearities for each converter.
The output of each module is given 
by:

\begin{equation}
M_{o_i}(x(t))=\begin{array}{l}
\alpha_{0_i}+ \alpha_{1_i} x(t)+\alpha_{2_i} x(t)^2+\alpha_{3_i} 
x(t)^3+\alpha_{4_i} x(t)^4+\alpha_{5_i} x(t)^5\end{array}
\end{equation}
Rather than use $0^{th}$ order parameters, a single DC offset parameter was 
used to capture otherwise unexplained DC on the output of the down-converter. 
The model was initialized to parameter values found by measuring one converter.
The algorithm was stopped several times during the experiment to perform model 
fitting. Each time, a non-linear solver was employed to find
%$\{\alpha_0{_a},\alpha_1{_a},\alpha_2{_a},\cdots,\alpha_1{_b},\alpha_2{_b},\cdots\}$
model parameter values that would yield the least total error power.
It should be noted that because 
the solver operates over the error PSD, it cannot distinguish between positive 
and negative $\alpha$ values (hereafter only absolute vales will be shown). 
During each call to the model fitting function, the top two stimuli were used 
in solving, and subsequent model fitting always included stimuli previously 
used in addition to those recently generated. 
Using a maximum stimulus bandwidth of 5 MHz, and an error BOI of 18 MHz,
RAVAGE was allowed to run for 471 generations and model fitting was attempted 3 
times.
Fig.\ref{updownupdownRes1} shows the progression of fitness and total error
power as the model was updated with new parameters.
\textit{Experiment 3} yielded 3 sets of calculated model parameters
presented in table \ref{table_parmVal1}.
The final model resulted in a maximum total error power 
of $3.841\times 10^{-3}$, down from the initial maximum total error power of 
$7.892\times 10^{-3}$.
By the noise floor standard set in \textit{Experiment 1}, all of these models 
exhibit total error powers far beneath those of \textit{Experiment 2}.
And finally, the performance specifications resulting from calculated 
parameters yield conversion-loss and $IIP_3$ figures consistent with the 
manufacturer specifications
of $8.7\,dB$ and +$34.6\,dBm$ for the up-converter, 
and $9.49\,dB$ and +$35\,dBm$ for the down-converter; according to Maxim's 
datasheet, the acceptable range is: $7.1\,dB$ of conversion loss, a 
+$33.5\,dBm$ $IIP_3$ during up-conversion, and a +$34.5\,dBm$ $IIP_3$ during 
down-conversion.

%\pagebreak
\subsection{Conclusion}
The RAVAGE algorithm was successful in creating stimuli that expose unmodeled 
behaviors in the both the composite \gls{awg}/\gls{digitizer} system, and the 
looped-back 
RF transceiver system. Though solution accuracy of model parameters cannot be 
guaranteed by RAVAGE's stimuli, we have shown that a better fitting model can 
be achieved.
It's reasonable to believe that if parameter's values are 
restricted within the solver to practical and realizable values, any 
improvement can be accepted as plausibly accounting for some behavioral 
difference.

\begin{figure}[!t]
	\centering
	\includegraphics[width=4in]{figures/Experiment3crop.pdf}
	\caption{Experiment \ref{exp:ravage2}(c) Results}
	\label{updownupdownRes1}
\end{figure}


\begin{table}
	\renewcommand{\arraystretch}{1.3}
	\caption{Model Parameter Back-Calculation Results}
	\label{table_parmVal1}
	\centering
	
	\begin{tabular}{c|ccccc}
		
		&Parameter & Model 1 & Model 2 & Model 3 & Model 4 \\
		\hline\
		
\multirow{5}{*}{\begin{sideways}\parbox{5mm}{Up-Converter}\end{sideways}}
		&$ \alpha_1  $&$  0.412                $&$ 0.354               $&$ 
		0.324               $&$ 0.367               $\\
		
		&$ \alpha_2  $&$ 1.54 \scriptstyle\times 10^{-2} $&$ 2.44 
		\scriptstyle\times 10^{-4} $&$ 6.77 \scriptstyle\times 10^{-3} $&$ 3.97 
		\scriptstyle\times 10^{-3} $\\
		
		&$ \alpha_3  $&$ 4.05 \scriptstyle\times 10^{-2} $&$ 1.05 
		\scriptstyle\times 10^{-3} $&$ 1.24 \scriptstyle\times 10^{-3} $&$ 1.69 
		\scriptstyle\times 10^{-3} $\\
		
		&$ \alpha_4  $&$ 0  $&$ 1.95 \scriptstyle\times 10^{-4} $&$ 3.95 
		\scriptstyle\times 10^{-4} $&$ 4.81 \scriptstyle\times 10^{-4} $\\
		
		&$ \alpha_5  $&$ 0 $&$ 4.87 \scriptstyle\times 10^{-5} $&$ 4.65 
		\scriptstyle\times 10^{-4} $&$ 3.78 \scriptstyle\times 10^{-4} $\\
		\hline
		
\multirow{6}{*}{\begin{sideways}\parbox{10mm}{Down-Converter}\end{sideways}}
		&$ \alpha_1  $&$  0.412                $&$ 0.354               $&$ 
		0.293               $&$ 0.335               $\\
		
		&$ \alpha_2  $&$ 1.54 \scriptstyle\times 10^{-2} $&$ 7.82 
		\scriptstyle\times 10^{-4} $&$ 6.42 \scriptstyle\times 10^{-3} $&$ 3.84 
		\scriptstyle\times 10^{-3} $\\
		
		&$ \alpha_3  $&$ 4.05 \scriptstyle\times 10^{-2} $&$ 1.02 
		\scriptstyle\times 10^{-3} $&$ 9.97 \scriptstyle\times 10^{-4} $&$ 1.41 
		\scriptstyle\times 10^{-3} $\\
		
		&$ \alpha_4  $&$ 0  $&$ 1.80 \scriptstyle\times 10^{-4} $&$ 9.60 
		\scriptstyle\times 10^{-4} $&$ 7.47 \scriptstyle\times 10^{-4} $\\
		
		&$ \alpha_5  $&$ 0 $&$ 4.37 \scriptstyle\times 10^{-5} $&$ 2.22 
		\scriptstyle\times 10^{-4} $&$ 1.72 \scriptstyle\times 10^{-4} $\\
		
		& DC offset & 0 & $10.31 mV $&$ 10.34 mV $&$ 10.36 mV $\\
		\hline
	\end{tabular}
\end{table}
