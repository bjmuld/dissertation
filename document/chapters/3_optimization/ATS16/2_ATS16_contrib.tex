% !TeX root = ../../../proposal_MASTER.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%   Chapter: Optimization Approaches
\section{Exploring Limits of Parametric Soft-Fault Simultaneous-Sensitivity}
\labelsec{ats}
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%\subsection{Summary}
In this section,
%, presented at the Asian Test Symposium, 2016,
I introduce a methodology for algorithmic generation of test signals for the
detection of short and open-circuit defects in analog
circuits \cite{muldreyConcurrentStimulusDefect2016}.
Prior algorithms have focused on test generation for specific short- 
or open-defect values assumed at the start which places the burden of failure 
coverage on accurate
analysis of observed defects in known failed parts and comes at a high cost.
In this work,
we optimize the test stimulus to be simultaneously sensitive to detecting the 
{\it weakest} shorts and opens in
analog circuits using a concurrent stimulus and defect value optimization
algorithm.
Since the defect value itself is an optimization parameter,
the responses of nonlinear circuits corresponding to {\it multiple
defect values} are considered as opposed to a {\it single} linearized
representation corresponding to a fixed defect value as in the existing state 
of
the art.
The algorithm produces a test stimulus along with values of the
weakest shorts and opens that the stimulus can detect (the locations of the
defects are specified to the algorithm).
These values are determined by the
design of the analog circuit itself and therefore subsume all specified
detectable defects for the circuits concerned.
Experimental results show  the
feasibility of the proposed approach on selected test cases and defect sets.

The objective of this work is, given an analog/mixed-signal/RF circuit and 
list of internal nodes, to design a test according to the criteria:
\begin{enumerate}
	\item {The device is simultaneously sensitive to any specified 
	short- and open-circuit faults.}
	\item {All shorts and opens should be sensed in their least severe 
	manifestations.}
	\item {Utilize primary inputs, power supply voltage, and loading of the 
	circuit under test to increase sensitivity.}
	\item{Observe only the \gls{dut}'s primary outputs.}
\end{enumerate}

The key advancement of this test generation algorithm over prior art 
is that existing test generation algorithms produce one test for one fault 
with 
a statically assigned magnitude, while our algorithm produces one test for 
many 
faults with dynamically determined minimal magnitudes. 
%Nonlinear systems mandate new techniques. 
For example, given a short-circuit defect modeled by 
the presence of an extraneous capacitor with value $C_{short}$, the 
sensitivities of the outputs of the circuit containing $C_{short}$ depend in 
general on the magnitude of $C_{short}$.
Therefore, in the presence of 
nonlinearities, the sensitivity can only be maximized for a given $C_{short}$, 
which is not known \textit{a priori}.) Existing test stimulus generation 
algorithms cannot address detection of defects across a large dynamic range of 
values of $C_{short}$, $R_{short}$, etc., due to linearization techniques that 
do not work well for devices with inherent nonlinearities ($R_{short}$ being 
another example of a circuit element used to modeling a defect).

\subsection{Algorithm}\labelsec{algorithm}

Statistical methods similar to those used in 
\cite{cherubal_test_2001,muldrey_ravage:_2013,banerjee_automatic_2011} are 
used to bring forward distinguishing subtleties in underlying circuit behaviors 
and leverage that information to predict other features of a circuit, in this 
case, the presence of a defect. In some cases, linear circuits can be made to 
operate in modes of 
reduced linearity in order to excite behaviors that are more sensitive to the 
presence of defects.
We conduct such a search over the space of input signals while 
simulating the circuit under varying fault parameterizations.
We draw attention to the fact that the circuit itself (i.e. 
faults/defects/process) is not a
point, but it is a continuous probability distribution.
Figure \ref{viz} illustrates an n-dimensional space of 
circuit outputs over varying stimulus, defect type, and defect magnitude.
For 
large circuits, sampling techniques must be utilized in selecting defects to 
simulate \cite{sunter_practical_2014,stratigopoulos_fast_2014}.
\begin{figure}
%    \centering
%	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[width=3in]{figures/loci-eps-converted-to}
		\caption{Stimulus Generation Problem Visualization}
		\label{viz}
%	\end{minipage}
\end{figure}
\begin{figure}
%	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[width=3.5in]{figures/algorithm-eps-converted-to}
		\caption{Test Generation Algorithm}
		\label{alg_fig}
%	\end{minipage}
\end{figure}


In order to simulate the circuit, we must model the defect within the circuit. 
For example, a short-circuit defect can be modeled by the presence of a single 
additional resistor within the circuit; it is parameterized by the 
resistance value, with a large value corresponding to a 
``mild'' short, and a small value corresponding to a ``severe'' short.
Designing a test for a short circuit is specific to the chosen model and 
parameterization; in the presence 
of nonlinearities, \textit{one cannot guarantee that the test holds for other 
parameterizations of the model without simulating them}.
Figure \ref{viz} is an example visualization of response loci for two 
circuits, 
faulty and fault-free, simulated over a continuum of defect parameter values 
for two different stimuli.
In order to detect a particularly parameterized defect, the corresponding 
point 
on its locus must be sufficiently distant from that of the defect-free circuit 
in the output space (where dimension $n$ corresponds to sample $n$ of the 
output waveform). In order to diagnose a defect, the corresponding point must 
not only be distant from that of the defect-free circuit, but it must also be 
sufficiently far from the loci of all parameterizations of all other defects 
considered. 

In choosing a stimulus for defect detection and defect diagnostic 
tests, our optimization favors those for which pairwise response distances are 
maximal over all defect parameterizations. That is, stimuli are optimized such 
that the points representing each device's (defect-free and defective) 
responses are vertices enclosing a maximal volume. Note that some optimization 
techniques can be confounded by the high-degree of nonlinearity of the volume 
metric (Equation \ref{volume_eq}); for this reason, and due to the underlying 
nonlinearities exploited, the genetic algorithm (GA) is a suitable choice for 
optimization.
\begin{equation}\label{volume_eq}
\text{n-dimensional volume} = \prod\limits_{i,j}  \text{dist}(x_i, x_j)
\end{equation}
Though large volumetric distance eases the tasks of both detection and 
diagnosis, it can be difficult to achieve and is not necessary; adequate 
distance in only one dimension is sufficient for distinguishing vectors. For 
this reason, the pairwise distance, $dist(x_1 , x_2)$ is not taken as the 
Euclidean norm, but rather higher order norms (Minkowski distance).
Individual samples of greater separation are better representations of response 
distance.
In the presented experiments, 
$p=4$.
%\begin{equation}
%	\label{distance_eq}
%	\text{dist}(x_i , x_j) = \Bigl\lvert \hat{x_i}-\hat{x_j} 
%	\Bigr\rvert_p\qquad(p>2)
%\end{equation}

At this point, it's worth noting that various statistical methods (PCA, SVD, 
etc.) have been used to reduce the dimensionality of output waveforms, and 
metrics in those spaces have been used as a distance metric
\cite{zhang_pca-based_2009,abu_el-yazeed_integrated_2003, zhang_wavelet_2009}.
This technique can be perilous for at least two reasons: many of those methods 
operate on centered and normalized data, so unless overall magnitudes are 
explicitly included, the inter-signal variances might not be large enough to 
measure; and secondly as mentioned earlier, sufficient distance in only one 
dimension is adequate.

Because each point in the output space requires one simulation, and many 
evaluations are required throughout the course of optimization, there is a 
clear need for reducing the dimensionality of the problem in some way. Many 
have proposed means of accomplishing reduction: linearization of different 
variables, considering a limited set of defects (dictionaries), some have 
assumed quadratic behavior of cost function, some have proposed random 
sampling, etc. It is clear that if anything 
useful is to result, the reduction of the dimensionality of the problem must 
be 
done very carefully to retain effectiveness and to produce a test which can be 
used in application. If done without care, one might face months of simulation 
time, might produce tests which cannot be seen beneath the instrumentation 
noise of the test equipment, or may find their test ineffective across the 
distributions of defects and circuits encountered in real life.

\textbf{The major contribution of this work is in providing a methodology for 
generating a test for detecting the presence of any single defect, 
parameterized to a minimally detectable state, from a set of parametric 
defects.}  One might assume that if the minimally invasive case is detectable, 
then defects with more egregious parameter values would also be detectable.  
In 
Section \refsec{algorithm} we describe the algorithm; in Section \ref{lna}, we 
examine an experimental test-case on a Low Noise Amplifier circuit; and in 
section \ref{filter_sec}, we examine an experimental test-case on a low-speed 
elliptic filter.

The proposed algorithm involves two loops. The inner loop for stimulus 
optimization is formulated as 
follows: Given a particular set of circuits including defective and 
defect-free, perform a search over operating conditions (supply, loading, 
etc.) 
and input stimuli simultaneously, such that the outputs of all circuits are 
maximally separable in as many dimensions as possible according to the fitness 
function described in Equation \ref{fitness_eq}.
The outer loop determines whether or not the differences among individual 
circuits' responses are detectable considering the noise floor of the circuit 
and test equipment.
For each defect instance, the minimum detectable value 
and the maximum undetectable magnitude of the defect is stored. 
The outer loop 
then derives a new set of defective instances with parametric values informed 
by the knowledge of the history of detectable and undetectable values, and 
re-enters the first 
loop (binary search). Upon termination, the last two tests applied are 
concatenated to yield a test for which all defects are detectable and the 
magnitudes of each defect's parameters are on the edge of detectability.

The genetic algorithm requires a scalar to represent the fitness of the  
population. We began with a Euclidean distance metric, but realized we required
bias towards the largest pairwise distance to reflect our criterion 
for detection (sufficient difference between any single pair of samples). 
That is, a better measure of the fitness of the stimulus in this context will 
be more sensitive to the dimension of maximal separation than to the mean 
separation over all dimensions (in this work $q=4$):

\begin{equation}
	\label{fitness_eq}
	\text{fitness} = {\lvert \hat{x_i}-\hat{x_j} \rvert}_q 
	\qquad{} 
	(q>4)
\end{equation}

The search space is quantized and bounded to limit optimization time. A single 
quantization level is chosen for all optimization dimensions. For large 
circuits, the number of interior nodes grows at least polynomially, and so 




\subsection{Completed Experiments}

\begin{figure}
	\centering
	\includegraphics[width=2.5in]{figures/opamp-eps-converted-to}
	\caption{Op-Amp test setup.}
	\label{opamp_fig}
\end{figure}

\paragraph{Operational Amplifier:}\label{opamp_sec}
In this experiment, a 100 MHz bipolar opamp spice model is used as the test 
vehicle.
Twenty faults were chosen at random from a complete set of node-to-node 
short-circuit faults and open-circuit faults at each terminal of each device.  
The comprehensive set totaled 406 possible unique short and open faults.
The opamp was configured with a single $50\Omega$ resistor in the negative 
feedback loop (Figure 3).
Input stimuli were provisioned at the inverting and 
non-inverting inputs of the amplifier, and an output load resistor of variable 
size was installed.
The algorithm was allowed to optimize VDD values, VSS values, load resistor 
values, and stimulus waveforms while comparing the responses of the 20 
faulty circuits under varying fault magnitudes. 
The resulting testbed parameters are:$ VDD = 5.76 V $, $ VSS = -6.43 V $,
$Rload = 4.49 k\Omega $.
Figure \ref{opamp_sigs_fig} shows the superimposed output waveforms of all 
faulty circuits at their minimal detectable magnitudes, and Table 
\ref{opamp_fault_tab} shows the minimal detectable values.  Figure 
\ref{opamp_stim_fig} shows the two stimulus signals used.

\begin{table}
	\caption{Minimally Detectable Opamp Fault Magnitudes}
	\label{opamp_fault_tab}
	\centering
	\singlespacing
	\footnotesize
	\begin{tabular}{|l|c|r||l|c|r|}
		Fault Name & Testable ? & Fault Magnitude & Fault Name & Testable ? & 
		Fault Magnitude \\
		\hline
		open 377  	&  yes &   154 $\Omega$ & short 258   &  no  &   -	\\
		open 333  	&  no  &   -  & short 135   &  no  &   -	\\
		open 356    &  yes &   154 $\Omega$	& short 317   &  yes &   80.6 
		M$\Omega$	\\
		short 95    &  yes &   80.6 M$\Omega$	& short 102   &  no  &   -	\\
		short 156   &  yes &   34 M$\Omega$	& short 134   &  no  &   -	\\
		short 6     &  yes &   80.6 M$\Omega$	& short 151   &  yes &   80.6 M 
		$\Omega$	\\
		short 293   &  yes &   65 M$\Omega$	& short 189   &  yes &   80.6 
		M$\Omega$	\\
		short 84    &  no  &   -& short 261   &  yes &   80.6 M$\Omega$	\\
		short 190   &  yes &   80.6 M$\Omega$	& short 124   &  yes &   80.6 
		M$\Omega$ \\
		short 195   &  yes &   80.6 M$\Omega$	\\
	\end{tabular}
\end{table}

\begin{figure}
	\centering
%	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[width=4.0in]{figures/opamp_responses-eps-converted-to}
		\caption{Responses of twenty marginally detectable faulty opamps, 
		Experiment \ref{filter_sec}(z).}
		\label{opamp_sigs_fig}
%	\end{minipage}
\end{figure}
%	\hfill
\begin{figure}
%	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[width=4.0in]{figures/opamp_stimulus-eps-converted-to}
		\caption{Test stimuli for opamp circuit, Experiment 
		\ref{opamp_sec}(a).}
		\label{opamp_stim_fig}
%	\end{minipage}
\end{figure}

\vfill

\paragraph{Low Noise Amplifier:}\label{lna}

In this experiment, a spice model of a 2.0 GHz \gls{lna} in 
180nm CMOS is used 
as a test-vehicle.
%As RF circuits are notoriously sensitive to parametric 
%variation, the experiment provides a nice test bed for the algorithm.
The design was first functionally verified in the defect free case, then seven 
copies of its netlist were created.  In each copy, an 
additional capacitor was introduced between an internal node and ground
and its value was left parameterizable.
The design operates on a nominal 3v supply, so bounds of [0 , 4] were used 
for \textit{vdd, vbias1} and \textit{vbias2}. An ideal quadrature modulator 
was 
used to create an RF signal from in-phase and quadrature baseband signals. An 
envelope following ("shooting") analysis was performed and the output envelope 
was measured for all defect instances. The baseband bandwidth was limited to 
the range (500kHz, 2 MHz) by choosing a sample rate of 4 MHz and a maximum 
simulation time $t_{max}=2 \text{us}$. The stimulus was a piecewise-linear 
waveform with 8 vertices, each vertex taking an amplitude in the range [0, 
0.5] 
volts and a time in the range [0, 2 us]. The output load was allowed to vary 
in 
the range [1, 1k] ohms. Each of these dimensions were discretized to 64 levels 
for the GA.
The minimum detectable capacitances through each iteration of the algorithm's 
outer-loop is shown in Figure \ref{lna_caps_fig}; the optimal stimulus and 
collection of responses are shown in Figure \ref{lna_stim_fig}. Results are 
summarized in Table \ref{lna_results_tab}.



\noindent
As described in Equation \ref{fitness_eq}, the GA effectively maximizes the 
minimum-dimensional-distances between defect responses and the defect-free 
response as calculated by Equation \ref{fitness_eq}. Table \ref{lna_tab} 
summarizes the parameters of the experiment.



\begin{table}
	\caption{Summary of LNA Experiment Parameters}
	\label{lna_tab}
	\centering
	\begin{tabular}{|l|c|c|c|}
		\hline
		\bfseries Parameter & \bfseries lower bound & \bfseries upper bound & 
		\bfseries quant. levels\\
		\hline
		$vdd$ 				& 0 v		& 4.0 v 		& 64\\
		$vbias1$				& 0	v		& 4.0 v		& 64\\
		$vbias2$				& 0	v		& 4.0 v		& 64\\
		$vin$ (amplitude)	& 0	v		& 0.5 v		& 64\\
		$vin$ (time)			& 0	s		& 2.0 us		& 64\\
		$vin$ (freq)			& 500 kHz	& 2 MHz 		& interp. by spice\\
		$R_{load}$			& 1 $\Omega$ 	& 1 k$\Omega$	& 64\\
		\hline
	\end{tabular}
\end{table}

\begin{figure}
	\centering
	\includegraphics[width=5.5in]{figures/lna_circuit-eps-converted-to}
	\caption{Low Noise Amplifier with Modeled Defects}
	\label{lnacircuit_fig}
\end{figure}


\begin{table}
	\singlespacing
	\footnotesize
	\caption{Summary of LNA Experiment Results}
	\label{lna_results_tab}
	\centering
	\begin{tabular}{|l|c|}
		\hline
		\bfseries Result & \bfseries Value\\
		\hline
		$vdd$ 				& 2.98 v \\
		$vbias1$				& 0.32 v \\
		$vbias2$				& 2.86 v \\
		$R_{load}$			& 667$\Omega$ \\
		$C_{min det.}$		& \{167, 31, n/a, 53, 53, 167, 32\} fF \\
		\hline
	\end{tabular}
\end{table}

\begin{figure}
	\centering
%	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[width=4.5in]{figures/lna_caps_combo-eps-converted-to}
		\caption{Evolution of Minimum Detectable Capacitance in LNA, Experiment
			\ref{lna}(b).}
		\label{lna_caps_fig}
	\end{figure}
%	\end{minipage}
%	\hfill
%	\begin{minipage}{0.49\textwidth}
\begin{figure}
		\centering
		\includegraphics[width=4.5in]{figures/lna_stimresp-eps-converted-to}
		\caption{Baseband Stimuli (and Responses) which Achieved Min. 
		Detectable Values in Experiment \ref{lna}(b)}
		\label{lna_stim_fig}
%	\end{minipage}
\end{figure}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Elliptic Filter:}
\label{filter_sec}

In this experiment, an elliptic \gls{lpf} from the ITC '97 benchmark 
circuits was used as a test-vehicle.
Given the 
particularly low frequency operation of this circuit, it was thought to test 
applicability of the algorithm to circuits with large time-constants.
The design was first functionally verified for the defect free case 
\cite{kaminska_analog_1997}, then fourteen copies of its
netlist were created.  In each copy, an additional capacitor was introduced 
between an internal node and ground and its value was left parameterizable.
The design operates on a a nominal 12v supply, so bounds of [0 , 12] and [0, 
-12] were used for \textit{vdd} and \textit{vss} respectively. The single 
input 
was directly driven from a 50$\Omega$ source. A standard transient analysis 
was 
performed and the filter output was measured for all defect instances. The 
baseband bandwidth was limited to the range (500 Hz, 100 kHz) by choosing a 
sample rate of 200 kHz and a maximum simulation time $t_{max}=2 \text{ms}$. 
The 
stimulus was a piecewise-linear waveform with 16 vertices, each taking an 
amplitude in the range [-10,10] volts, and time [0, 2] milliseconds; the 
output 
load was allowed to vary in the range [1, 10k] ohms. Each of these dimensions 
were discretized to 64 levels for the GA.
As described in Equation \ref{fitness_eq}, the GA effectively maximizes the 
minimum-dimensional-distances between defect responses and the defect-free 
response as calculated by Equation \ref{fitness_eq}.
The minimum detectable capacitances through each iteration of the algorithm's 
outer-loop are shown in Figure \ref{filter_caps_fig}. Results are summarized in 
Table \ref{filter_results_tab}.

\begin{figure}
	\centering
	\includegraphics[width=6.0in]{figures/elliptic_circuit-eps-converted-to}
	\caption{Elliptic Filter with Modeled Defects \cite{kaminska_analog_1997}}
	\label{filter_fig}
\end{figure}

\noindent


\begin{table}
	\caption{Summary of Elliptical Filter Experiment Parameters}
	\label{filter_tab}
	\centering
	\begin{tabular}{|l|c|c|c|}
		\hline
		\bfseries Parameter & \bfseries lower bound & \bfseries upper bound & 
		\bfseries quant. levels\\
		\hline
		$vdd$ 				& 0 v		& 12 v 		& 64\\
		$vss$				& -12	v		& 0 v		& 64\\
		$vin$ (amplitude)	& -10	v		& 10 v		& 64\\
		$vin$ (time)			& 0	s		& 2.0 ms		& 64\\
		$vin$ (freq)			& 500 Hz	 & 100 kHz 		& interp. by spice\\
		$R_{load}$			& 1 $\Omega$ 	& 10 k$\Omega$	& 64\\
		\hline
	\end{tabular}
\end{table}



\begin{table}
	\caption{Summary of Elliptic Filter Experiment Results}
	\label{filter_results_tab}
	\centering
	\begin{tabular}{|l|c|}
		\hline
		\bfseries Result & \bfseries Value\\
		\hline
		$vdd$ 				& 6.09 v \\
		$vss$				& -9.33 v \\
		$R_{load}$			& 4.6 k$\Omega$ \\
		$C_{min det.}$		& \{155p, 1.7n, 1.7n, 4.9u, 667p, 784p, 253p\\
		&						100n, 509n, 0.9, 252p, 667p, 369n,0.92\} F \\
		\hline
	\end{tabular}
\end{table}

\begin{figure}
	\centering
%	\begin{minipage}{0.49\textwidth}
		\centering
		\includegraphics[width=4.5in]{figures/filtercaps_combo-eps-converted-to}
		\caption{Evolution of Minimum Detectable Capacitance in Elliptical 
		Filter, Experiment \ref{filter_sec}(c)}
		\label{filter_caps_fig}
\end{figure}
%	\end{minipage}
%	\hfill
%	\begin{minipage}{0.49\textwidth}
\begin{figure}
		\centering
		\includegraphics[width=4.5in]{figures/filter_stimresps-eps-converted-to}
		\caption{Stimulus (and Responses) which Achieved Min. Detectable Values 
		for Elliptic Filter, Experiment \ref{filter_sec}(c)}
		\label{filt_stim_fig}
%	\end{minipage}
\end{figure}

\subsection{Conclusion}

Some capacitive shorts to ground were undetectable, most likely because of the
low (zero) source impedance found at supply nodes of the simulation model.
Other tests were sensitive to very small capacitances relative to the design
values, in some cases down to the tens of femtofarad.
The results were carefully reviewed and it's 
likely attributed to absence of device noise in 
the simulation.
Given the magnitude of 
the design capacitances (2.7 nF) in the elliptic filter, it was expected that 
the minimum 
detectable values be found to be no smaller than 1 nF or so, yet the 
expectation was surpassed.Additionally,
Looking at values of supply and bias voltages at which the solver converged,
It is apparent that a state of reduced linearity has aided in the successful 
detection of 
the capacitive defects, perhaps hinting that a degree of harmonic generation 
and intermodulaion in the mosfets is aiding in sensitizing nodes to small 
capacitive loads.
Additionally, investigation of the maximally advantageous way to apply 
this technique in diagnosis must be explored.
In these simulations, a binary 
search was conducted from a minimum-capacitance initial condition. While the 
searches did converge, they did so in leaps and bounds, and thus future work 
will 
look into means of ensuring some way of quantifying certainties in defect 
magnitudes between sample points. Including device noise could have provided a 
better understanding of how realistic these measurements might be, however it 
would come at added cost in simulation time. Finally, a look at the 
effectiveness of various classifiers in distinguishing among differently 
loaded 
nodes and nodes loaded with capacitances of large variation will be explored.
